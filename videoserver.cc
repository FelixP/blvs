#define DO_TIMING

// #define V4L2C_USE_READ
// #define V4L2C_USE_YU12
// #define NO_LIBV4L2
// #define V4L2C_CLOSE_TO_RATE

// #define SW
#define YUYV2RGB

#include "boost/thread/thread.hpp"
#include "boost/thread/mutex.hpp"

#include "common/verbosity.h"
#include "common/execution_timing.h"

// Image sources
#include "capture/v4l2capture.h"
#include "common/ImageFileStream.h"

// Image sinks
#include "pgf/PGFEncoder.h"
#include "common/FileImage.h"
#include "common/ImageSinkSplitter.h"

// Video publishing
#include "afp/AFPPublisherEventChannel.h"
#include "famouso_bindings.h"

// Util: needed by FileChecker
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>



typedef afp::frag::RedundancyAttribute<afp::DefaultTag, 0> PublishFECRedundancy;

typedef afp::AFPConfig <
	afp::ApplicationLayerDimensions,
	afp::FragUseEventSeq,
	afp::FragUseFEC,
	afp::DefragSupportEventSeq,
	afp::DefragDuplicateChecking,
	afp::DefragSupportFEC,
	PublishFECRedundancy
> MyAFPConfig;



// Video-ID constists of 7 chars and is used to construct famouso subject names
// for publishing the video stream
const char * default_video_id = "blvs!!!";

// Default quality used for PGF encoding
const int default_quality = 5;
 


volatile int done;

void siginthandler(int egal)
{
	done = 1;
}

class FileChecker {
public:
	enum FileType { not_found, regular_file, directory, block_device, char_device, pipe, symlink, socket, unknown };

	static FileType get_file_type(const char * path)
	{
		struct stat sb;

		if (stat(path, &sb) == -1) {
			return not_found;
		}

		switch (sb.st_mode & S_IFMT) {
			case S_IFBLK:  return block_device;
			case S_IFCHR:  return char_device;
			case S_IFDIR:  return directory;
			case S_IFIFO:  return pipe;
			case S_IFLNK:  return symlink;
			case S_IFREG:  return regular_file;
			case S_IFSOCK: return socket;
		}

		return unknown;
	}
};


class LevelPublisher {

	// Static storage for video_id, used for constructing channel subjects
	static const char *& video_id()
	{
		static const char * id = 0;
		return id;
	}

	// Static storage for next_channel_id, used for constructing channel subjects
	static char & next_channel_id()
	{
		static char id = '0';
		return id;
	}

	static const char * get_next_subject() {
		static char s [8];
		memcpy(&s, video_id(), 7);
		s[7] = next_channel_id()++;
		return s;
	}

	typedef afp::AFPPublisherEventChannel<famouso::config::PEC, MyAFPConfig> AFPPEC;

	class Channel : public AFPPEC {
	public:
		afp::LargeEvent event;

		Channel()
			: AFPPEC(get_next_subject()),
			  event(AFPPEC::subject())
		{
			announce();
		}
	};

	Channel * publisher_channels;

public:
	LevelPublisher(const char * video_id)
	{
		LevelPublisher::video_id() = video_id;
		publisher_channels = new Channel [MyPGFImage::levels];
		last_instance() = this;
		famouso::init<famouso::config>();
	}

	~LevelPublisher()
	{
		delete [] publisher_channels;
	}

	static LevelPublisher *& last_instance()
	{
		static LevelPublisher * p = 0;
		return p;
	}

	/*!
	 *
	 *	\param	level	Level, in ascending resolution: 0, 1, ..., MyPGFImage::levels-1
	 */
	static void level_encoding_completed_callback(CPGFStream * stream, UINT8 level, UINT64 start_pos, UINT64 end_pos)
	{
		Channel & c = last_instance()->publisher_channels[level];

		c.event.data = stream->GetBuffer() + start_pos;
		c.event.length = end_pos - start_pos;

		VERBOSITY_5(printf("publish event of size %i (%i - %i) on level channel %i\n", (int)c.event.length, (int)start_pos, (int)end_pos, level));

		if (level == 0)
			PublishFECRedundancy::value() = 5;
		else
			PublishFECRedundancy::value() = 0;

		c.publish(c.event);
	}
};


class VideoServer
{
	clock_member_decl(frame_to_frame_time_sum);
	clock_member_decl(frames);

public:

	VideoServer()
	{

		clock_member_init(frame_to_frame_time_sum);
		clock_member_init(frames);
	}

	~VideoServer()
	{
		VERBOSITY_1(clock_mean_msg(frame_to_frame_time_sum, frames, "(VideoServer)\tDurchschnittsinterframezeit\t"));
	}

	template <class ImageStream>
	void run(ImageStream & stream, int quality, const char * video_id, const char * record_dir)
	{
		char * record_filepath_buffer = 0;	// Buffer to build path/filenames for recording
		char * record_filename_buffer = 0;	// Points into record_filepath_buffer where filename has to be written
		unsigned int record_counter = 0;	// Counter for numbering recorded images

		// TODO: use verbose
		done = 0;
		signal(SIGINT, siginthandler);
		
		if (record_dir)
		{
			unsigned int dir_len = strlen(record_dir);
			record_filepath_buffer = new char [dir_len + 50];
			if (!record_filepath_buffer) {
				perror("no memory");
				exit(-1);
			}
			strcpy(record_filepath_buffer, record_dir);
			record_filename_buffer = record_filepath_buffer + dir_len;
		}

		LevelPublisher pub(video_id);

		PGFEncoder PGFenc;
		PGFenc.set_quality(quality);

		clock_decl(frame_time);
		clock_start(frame_time);

		while(!done)
		{
			// TODO: capture/encoding on demand

			if (!record_dir) {
				// Get image from source and put it into the encoder
				stream.give_image(PGFenc);
			} else {
				// Get image from source and put it into the encoder and the file exporter
				snprintf(record_filename_buffer, 50, "image_%08u.yuyv", record_counter++);
 				FileImage file(record_filepath_buffer);
				ImageSinkSplitter<PGFEncoder, FileImage> encoder_and_file(PGFenc, file);
 				stream.give_image(encoder_and_file);
			}

			// Encode image, publish levels via level_encoding_completed_callback
			PGFenc.encode(&LevelPublisher::level_encoding_completed_callback);

			clock_stop_sum(frame_time,frame_to_frame_time_sum);clock_start(frame_time);
			timing_command(frames++);
		}
		
		if (record_filepath_buffer)
			delete [] record_filepath_buffer;
	}
};

int main (int argc, char **argv)
{
	const char * image_source = 0;
	int quality = default_quality;
	const char * record_dir = 0;
	const char * video_id = default_video_id;

	bool wrong_args = (argc < 2);
	int arg = 1;
	enum { other_arg, quality_arg, id_arg, record_dir_arg, verbosity_arg } next_arg = other_arg; 

	while (!wrong_args && arg < argc) {
		if (argv[arg][0] == '-') {
			if (strcmp(argv[arg] + 1, "-quality") == 0)
				next_arg = quality_arg;
			else if (strcmp(argv[arg] + 1, "-id") == 0)
				next_arg = id_arg;
			else if (strcmp(argv[arg] + 1, "-verbosity") == 0)
				next_arg = verbosity_arg;
			else if (strcmp(argv[arg] + 1, "v") == 0)
				next_arg = verbosity_arg;
			else if (strcmp(argv[arg] + 1, "-record") == 0)
				next_arg = record_dir_arg;
			else
				wrong_args = true;
		} else {
			if (next_arg == id_arg) {
				video_id = argv[arg];
				int len = strlen(video_id);
				if (len != 7) {
					fprintf(stderr, "Error: Length of Video-ID must be 7 byte.\n");
					wrong_args = true;
				}
				
			} else if (next_arg == quality_arg) {
				quality = atoi(argv[arg]);
				if (quality < 0 || quality > 30) {
					fprintf(stderr, "Error: Quality has to be between 0 and 30.\n");
					wrong_args = true;
				}
			} else if (next_arg == verbosity_arg) {
				if(SET_VERBOSITY(atoi(argv[arg])))wrong_args = true;
			} else if (next_arg == record_dir_arg) {
				record_dir = argv[arg];
			} else {
				if (image_source) {
					fprintf(stderr, "Error: Only one image source allowed.\n");
					wrong_args = true;
				}
				image_source = argv[arg];
			}
			next_arg = other_arg;
		}
		arg++;
	}

	if (wrong_args || next_arg != other_arg || image_source == 0) {
		fprintf(stderr, 
			"Usage:\n"
			"    videoserver VIDEO_DEV [OPTIONS]\n"
			"    videoserver IMAGE_DIR [OPTIONS]\n"
			"\n"
			"  VIDEO_DEV: video device file\n"
			"  IMAGE_DIR: directory containing 640x480 YUYV image files\n"
			"\n"
			"  OPTIONS:\n"
			"    --quality Q     Set quality to use for PGF encoding. 0 <= Q <= 30\n"
			"                    Default quality: %i\n"
			"    --id VIDEO_ID   Set ID used for publishing the video stream via\n"
			"                    the famouso middleware. It has to be a string of\n"
			"                    7 bytes. Default Video-ID: %s\n"
			"   --quiet          Do not be verbose.\n"
			"   --record TO_DIR  Save video frames as YUYV image files in directory\n"
			"                    TO_DIR.\n"
			"   --verbosity LEVEL Verbositylevel (0 - 5)\n"
			"   -v LEVEL         Verbositylevel (0 - 5)\n"
			
			"\n"
			, default_quality
			, default_video_id
		);
		return -1;
	}

	FileChecker::FileType ft = FileChecker::get_file_type(image_source);
	if (ft == FileChecker::not_found) {
		fprintf(stderr, "Error: Cannot find image source %s.\n", image_source);
		return -1;
	} else if (ft == FileChecker::directory) {
		// Use directory of as image source
		ImageFileStream stream(image_source, 640, 480);
		VideoServer server;
		server.run(stream, quality, video_id, record_dir);
	} else {
		// Use video device as image source
		int vd = open(image_source, O_RDWR);
		if (vd < 0) {
			perror("open video device");
			return -1;
		}
		V4l2ImgStream stream(vd);
		if(!stream.set_capture_format(640, 480) || !stream.start_streaming()) {
			perror("init video device");
			return -1;
		}
		VideoServer server;
		server.run(stream, quality, video_id, record_dir);
		stream.stop();
	}
	
	usleep(100000);
	return 0;
}

