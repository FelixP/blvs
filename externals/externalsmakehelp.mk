.PHONY: _all

# Ensure that first rule is all
_all: all


ifneq ($(wildcard config.mk),)
include config.mk
endif

EXTERNALS_PATH ?= $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
EXTERNALS_PATH := $(EXTERNALS_PATH:/=)

###############################################################################
#  Famouso  ###################################################################

FAMOUSO_INSTALLDIR ?= $(EXTERNALS_PATH)/famouso

FAMOUSO_CONFIG = linux
include $(FAMOUSO_INSTALLDIR)/make/famouso_as_external_lib.mk

FAMOUSO ?= $(FAMOUSO_COMPILER_OPTIONS_MUST_HAVE) $(EXTERNALS_PATH)/vdmfec/fec.o

FAMOUSO_BOOST_ONLY ?= -I$(INCDIR) $(LIBBOOST)

###############################################################################
#  CImg  ######################################################################

CIMG ?= -I$(EXTERNALS_PATH) -L/usr/X11R6/lib -lm -lX11 -lpthread


##############################################################################
#  libpgf  ###################################################################

PGF ?= -I$(EXTERNALS_PATH)/../pgf/patched-libpgf/include $(EXTERNALS_PATH)/../pgf/patched-libpgf/src/.libs/libpgf.a
PGF_DEP = build_patched_pgflib_rule

build_patched_pgflib_rule : 
	make -f runautomake -C $(EXTERNALS_PATH)/../pgf/patched-libpgf/ CONFIGURE_FLAGS='CFLAGS="$(DEBUG)" CXXFLAGS="$(DEBUG)"'


##############################################################################
#  libv4l2  ##################################################################

V4LSTATIC ?= no

ifneq ($(or $(findstring no,$(V4LSTATIC)), $(findstring false,$(V4LSTATIC))),)
	V4L2 ?= -I$(EXTERNALS_PATH)/libv4l/include -L$(EXTERNALS_PATH)/libv4l/libv4l2/ -L$(EXTERNALS_PATH)/libv4l/libv4lconvert/ -lv4l2 -lv4lconvert
else
	V4L2 ?= -I$(EXTERNALS_PATH)/libv4l/include $(EXTERNALS_PATH)/libv4l/libv4l2/libv4l2.a $(EXTERNALS_PATH)/libv4l/libv4lconvert/libv4lconvert.a -lrt
endif
