#define DO_TIMING

#include "CImg/CImg.h"

#include "PGFEncoder.h"
#include "PGFDecoder.h"

#define YUYV2RGB
#include "../common/CImgIM.h"
#include "../common/execution_timing.h"


int main(int argc, char ** argv)
{
	if (argc != 2) {
		fprintf(stderr, "Usage: pgftest IMAGE_FILE\n");
		return -1;
	}

	CImgIM in_image;
	in_image.assign (argv[1]);

	CImgIM out_image;
 	cimg_library::CImgDisplay disp(in_image.dimx(), in_image.dimy(), "PGF", 1);
	
	PGFEncoder enc;
	PGFDecoder dec;

	bool something_changed = true;
	int quality = 0;

	do
	{
		if (something_changed)
		{
			// Encode
			enc.set_quality(quality);
			in_image.give_image(enc);
			clock_start_decl(enc_time);
			enc.encode();
			clock_stop_msg(enc_time, "Encoding time");

			// Decode
			clock_start_decl(dec_time);
			dec.decode(enc.get_data(), enc.get_size());
			clock_stop_msg(dec_time, "Decoding time");
			dec.give_image(out_image);
			
			// Display result
			disp.display(out_image);
			
			something_changed = false;
		}

		if (disp.button)
		{
			something_changed = true;
			if (disp.button == 1)
				quality++;
			else
				quality--;
			quality = (quality + 31) % 31;
		}

		if (disp.is_resized) {
			disp.resize();
			disp.display(out_image);
		}

		disp.wait();
	}
	while (!disp.is_closed);
			
	return 0;
}

