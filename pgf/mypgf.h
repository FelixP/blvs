#pragma once

#include "libpgf/PGFimage.h"
#include "libpgf/Decoder.h"
#include "libpgf/Encoder.h"

#include "../common/execution_timing.h"
#include "../common/verbosity.h"

// CPGFImage erweitert um progressive encoding und direktem YUV-Import
// Use 2 geyscale channels of same size:
// 1. Y channel
// 2. undersampled U channel and undersampled V channel
class MyPGFImage : public CPGFImage {

	UINT8 Clamp(DataT v) const {
		// needs only one test in the normal case
		if (v & 0xFFFFFF00) return (v < 0) ? (UINT8)0 : (UINT8)255; else return (UINT8)v;
	}

public:
	struct YUYV {uint8_t y0 ,u0 ,y1 ,v0;} __attribute__((packed));
	struct YU12 {uint8_t* y ,*u ,*v; enum{uscaleh=1,uscalev=1,vscaleh=1,vscalev=1};};

	void ImportYUYVBitmap(UINT8 *buff) THROW_ {
		ASSERT(buff);
		ASSERT(m_channel[0]);
		ASSERT(m_header.mode == ImageModeGrayScale);
		ASSERT(m_header.channels == 3);
		ASSERT(m_header.width%2 == 0);
		ASSERT(m_header.height%2 == 0);

		DataT* y = m_channel[0]; ASSERT(y);
		DataT* u = m_channel[1]; ASSERT(u);
		DataT* v = m_channel[2]; ASSERT(v);
		m_width[0] = m_header.width;
		m_height[0] = m_header.height;
		m_width[1] = m_header.width/2;
		m_height[1] = m_header.height/2;
		m_width[2] =m_header.width/2;
		m_height[2]= m_header.height/2;
		
		const UINT32 width = m_header.width;
		const UINT32 height = m_header.height;
		const UINT32 half_width = width >> 1;
		const UINT32 half_height = height >> 1;

		YUYV *inPic_l1 = (YUYV*) buff;
		YUYV *inPic_l2 = inPic_l1 + half_width;

		DataT* y_l1 = y;
		DataT* y_l2 = y + m_header.width;

		// copy macropixel
		for(unsigned int line = 0; line < half_height; line++)
		{
			for(unsigned int pos = 0; pos < half_width; pos++)
			{
				*(y_l1++) = inPic_l1->y0;
				*(y_l1++) = inPic_l1->y1;

				*(y_l2++) = inPic_l2->y0;
				*(y_l2++) = inPic_l2->y1;

				#if 1
				*(u++) = inPic_l1->u0;
				*(v++) = inPic_l1->v0;
				#else
				*(u++) = (inPic_l1->u0 + inPic_l2->u0)/2;
				*(v++) = (inPic_l1->v0 + inPic_l2->v0)/2;
				#endif

				inPic_l1++;
				inPic_l2++;
			}
			y_l1 += width;
			y_l2 += width;
			inPic_l1 += half_width;
			inPic_l2 += half_width;
		}
	}
	
	void ImportYU12toYUYVBitmap(YU12 *buff) THROW_ {
		ASSERT(buff);
		ASSERT(m_channel[0]);
		ASSERT(m_header.mode == ImageModeGrayScale);
		ASSERT(m_header.channels == 3);
		ASSERT(m_header.width%2 == 0);
		ASSERT(m_header.height%2 == 0);

		DataT* y = m_channel[0]; ASSERT(y);
		DataT* u = m_channel[1]; ASSERT(u);
		DataT* v = m_channel[2]; ASSERT(v);
		m_width[0] = m_header.width;
		m_height[0] = m_header.height;
		m_width[1] = m_header.width/2;
		m_height[1] = m_header.height/2;
		m_width[2] =m_header.width/2;
		m_height[2]= m_header.height/2;
		uint8_t *iny = buff->y;
		uint8_t *inu = buff->u;
		uint8_t *inv = buff->v;
		for(unsigned int i = m_header.height/2 * m_header.width/2;i;i--)
		{
			*y++ = *iny++;
			*y++ = *iny++;
			*y++ = *iny++;
			*y++ = *iny++;
			*u++ = *inu++;
			*v++ = *inv++;
		}
	}
	
	void ExportYUYV(uint8_t * data)
	{
		ASSERT(buff);
		ASSERT(m_channel[0]);
		ASSERT(m_header.mode == ImageModeGrayScale);
		ASSERT(m_header.channels == 3);
		ASSERT(m_header.width%2 == 0);

		DataT* y = m_channel[0]; ASSERT(y);
		DataT* u = m_channel[1]; ASSERT(u);
		DataT* v = m_channel[2]; ASSERT(v);

		const UINT32 width = Width(Level());;
		const UINT32 height = Height(Level());
		const UINT32 half_width = width >> 1;
		const UINT32 half_height = height >> 1;

		YUYV *inPic_l1 = (YUYV*) data;
		YUYV *inPic_l2 = inPic_l1 + half_width;

		DataT* y_l1 = y;
		DataT* y_l2 = y + width;

		// kopiere macropixel
		for(unsigned int line = 0; line < half_height; line++)
		{
			for(unsigned int pos = 0; pos < half_width; pos++)
			{
				inPic_l1->y0 = Clamp(*(y_l1++));
				inPic_l1->y1 = Clamp(*(y_l1++));
				inPic_l2->y0 = Clamp(*(y_l2++));
				inPic_l2->y1 = Clamp(*(y_l2++));
				
				inPic_l1->u0 = *u;
				inPic_l1->v0 = *v;
				inPic_l2->u0 = *u;
				inPic_l2->v0 = *v;

				inPic_l1++;
				inPic_l2++;
				u++;
				v++;
			}
			y_l1 += width;
			y_l2 += width;
			inPic_l1 += half_width;
			inPic_l2 += half_width;
		}
	}

	void ExportYU12(uint8_t * data)
	{
		ASSERT(buff);
		ASSERT(m_channel[0]);
		ASSERT(m_header.mode == ImageModeGrayScale);
		ASSERT(m_header.channels == 3);
		ASSERT(m_header.width%2 == 0);

		DataT* y = m_channel[0]; ASSERT(y);
		DataT* u = m_channel[1]; ASSERT(u);
		DataT* v = m_channel[2]; ASSERT(v);

		const UINT32 width = Width(Level());;
		const UINT32 height = Height(Level());

		uint8_t *outPic = data;

		// kopiere Y
		for(unsigned int i = width * height; i; i--)
		{
			*outPic++ = Clamp(*y++);
		}
		// kopiere U
		for(unsigned int i = (width * height)>>2; i; i--)
		{
			*outPic++= Clamp(*u++);
		}
		// kopiere V
		for(unsigned int i = (width * height)>>2; i; i--)
		{
			*outPic++= Clamp(*v++);
		}
	}
	
	void ExportYU12(uint8_t * data, int level)
	{
		ASSERT(buff);
		ASSERT(m_channel[0]);
		ASSERT(m_header.mode == ImageModeGrayScale);
		ASSERT(m_header.channels == 3);
		ASSERT(m_header.width%2 == 0);
		
		DataT* y = m_channel[0]; ASSERT(y);
		DataT* u = m_channel[1]; ASSERT(u);
		DataT* v = m_channel[2]; ASSERT(v);
		
		const UINT32 width = Width(level);;
		const UINT32 height = Height(level);
		const UINT32 cwidth = width >> 1;;
		const UINT32 cheight = height >> 1;
		
		int dlevel = Level() - level;
		dlevel = dlevel > 0 ? dlevel : 0;
		uint8_t *outPic = data;
		
		// kopiere Y
		for(unsigned int line = 0; line < height; line++)
		{
			unsigned int mdline = width * line;
			unsigned int msline = (width >> dlevel) * (line >> dlevel);
			for(unsigned int pos = 0;pos < width ;pos++)
				outPic[mdline + pos] = Clamp(y[msline + (pos >> dlevel)]);
		}
		// kopiere U
		outPic += height*width;
		for(unsigned int line = 0; line < cheight; line++)
		{
			unsigned int mdline = cwidth * line;
			unsigned int msline = (cwidth >> dlevel) * (line >> dlevel);
			for(unsigned int pos = 0;pos < cwidth ;pos++)
				outPic[mdline + pos] = Clamp(u[msline + (pos >> dlevel)]);
		}
		// kopiere V
		outPic += cheight*cwidth;
		for(unsigned int line = 0; line < cheight; line++)
		{
			unsigned int mdline = cwidth * line;
			unsigned int msline = (cwidth >> dlevel) * (line >> dlevel);
			for(unsigned int pos = 0;pos < cwidth ;pos++)
				outPic[mdline + pos] = Clamp(v[msline + (pos >> dlevel)]);
		}
	}
	
	enum { levels = 4 };
	
	// 0 <= level < levels (0: Level with smallest resolution)
	typedef void (* LevelEncodedCallback)(CPGFStream * stream, UINT8 level, UINT64 start_pos, UINT64 end_pos);


	void Write(CPGFStream* stream, LevelEncodedCallback level_complete_cb, UINT8 image_id) THROW_ {
		ASSERT(level_complete_cb);
		ASSERT(stream);
		int i;
		UINT64 level_start;

		m_header.nLevels = levels;
		m_quant = m_header.quality;

		ASSERT(m_header.nLevels <= MaxLevel);
		ASSERT(m_header.quality <= MaxQuality); // quality is already initialized

		SUPPORT_VERBOSITY_5(clock_start_decl(timing_var1));
		// create new wt channels
		for (i=0; i < m_header.channels; i++) {
			m_wtChannel[i] = new CWaveletTransform(m_width[i], m_height[i], levels, m_channel[i]);
			if (!m_wtChannel[i]) {
				ReturnWithError(InsufficientMemory);
			}

			// wavelet subband decomposition 
			for (m_currentLevel=0; m_currentLevel < levels; m_currentLevel++) {
				m_wtChannel[i]->ForwardTransform(m_currentLevel);
			}
		}
		VERBOSITY_5(clock_stop_msg(timing_var1, "wavlet transformation"));

		// First level includes image header
		level_start = stream->GetPos();
		
		// open encoder and eventually write headers and levelLength
		CEncoder encoder(stream, m_header, m_levelLength, image_id);

		// encode quantized wavelet coefficients and write to PGF file
		// encode subbands, higher levels first
		// color channels are interleaved

		SUPPORT_VERBOSITY_5(clock_start_decl(timing_var2));
		// encoding scheme without ROI
		m_currentLevel = (UINT8)levels;
		encoder.WriteLevelHeader();
		for (i=0; i < m_header.channels; i++) {
			m_wtChannel[i]->GetSubband(m_currentLevel, LL)->ExtractTile(encoder, m_quant);
			//encoder.EncodeInterleaved(m_wtChannel[i], m_currentLevel, m_quant); // until version 4
			m_wtChannel[i]->GetSubband(m_currentLevel, HL)->ExtractTile(encoder, m_quant); // since version 5
			m_wtChannel[i]->GetSubband(m_currentLevel, LH)->ExtractTile(encoder, m_quant); // since version 5
			m_wtChannel[i]->GetSubband(m_currentLevel, HH)->ExtractTile(encoder, m_quant);
		}
		// all necessary data buffered!
		encoder.FlushLevel();
		VERBOSITY_5(clock_stop_msg(timing_var2, "encoding level"));
		SUPPORT_VERBOSITY_5(clock_start(timing_var2));
		level_complete_cb(stream, 0, level_start, stream->GetPos());
		VERBOSITY_5(clock_stop_msg(timing_var2, "level callback"));
		m_currentLevel--;

		for (; m_currentLevel > 0; m_currentLevel--) {
			SUPPORT_VERBOSITY_5(clock_start(timing_var2));
			level_start = stream->GetPos();
			encoder.WriteLevelHeader();
			for (i=0; i < m_header.channels; i++) {
				//encoder.EncodeInterleaved(m_wtChannel[i], m_currentLevel, m_quant); // until version 4
				m_wtChannel[i]->GetSubband(m_currentLevel, HL)->ExtractTile(encoder, m_quant); // since version 5
				m_wtChannel[i]->GetSubband(m_currentLevel, LH)->ExtractTile(encoder, m_quant); // since version 5
				m_wtChannel[i]->GetSubband(m_currentLevel, HH)->ExtractTile(encoder, m_quant);
			}
			// all necessary data of a level buffered!
			encoder.FlushLevel();
			VERBOSITY_5(clock_stop_msg(timing_var2, "encoding level"));
			SUPPORT_VERBOSITY_5(clock_start(timing_var2));
			level_complete_cb(stream, levels-m_currentLevel, level_start, stream->GetPos());
			VERBOSITY_5(clock_stop_msg(timing_var2, "level callback"));
		}
	}

	/////////////////////////////////////////////////////////////////////////////
	// Open a PGF image at current stream position: header
	// Precondition: The stream has been opened for reading.
	// It might throw an IOException.
	// @param stream A PGF stream
	void StartRead(CPGFStream *stream, const PGFHeader & header) THROW_ {
		ASSERT(stream);

		m_header = header;
		m_header.nLevels = levels;

		if (m_decoder) delete m_decoder;
		m_decoder = new CDecoder(stream, m_header);
		if (!m_decoder) ReturnWithError(InsufficientMemory);

		if (m_header.nLevels > MaxLevel) ReturnWithError(FormatCannotRead);

		m_currentLevel = m_header.nLevels;
		m_width[0] = m_header.width;
		m_height[0] = m_header.height;
		for (int i=1; i < m_header.channels; i++) {
			m_width[i] = m_width[0]/2;
			m_height[i] = m_height[0]/2;
		}
		m_quant = m_header.quality;

		// init wavelet subbands
		for (int i=0; i < m_header.channels; i++) {
			m_wtChannel[i] = new CWaveletTransform(m_width[i], m_height[i], m_header.nLevels);
			if (!m_wtChannel[i]) ReturnWithError(InsufficientMemory);
		}

		ASSERT(m_decoder);
	}
	
	/*!
	 *	\brief	Decode everything up to level
	 */
	void Read(int level = 0) THROW_ {
		ASSERT((level >= 0 && level < m_header.nLevels));
		ASSERT(m_decoder);
		int i;

		// encoding scheme without ROI
		while (m_currentLevel > level) {
			UINT8 image_id;
			m_decoder->ReadLevelHeader(image_id, levels - m_currentLevel);
			// Read current level number of debugging
			for (i=0; i < m_header.channels; i++) {
				ASSERT(m_wtChannel[i]);
				// decode file and write stream to m_wtChannel
				if (m_currentLevel == m_header.nLevels) { 
					// last level also has LL band
					m_wtChannel[i]->GetSubband(m_currentLevel, LL)->PlaceTile(*m_decoder, m_quant);
				}
				m_wtChannel[i]->GetSubband(m_currentLevel, HL)->PlaceTile(*m_decoder, m_quant);
				m_wtChannel[i]->GetSubband(m_currentLevel, LH)->PlaceTile(*m_decoder, m_quant);
				m_wtChannel[i]->GetSubband(m_currentLevel, HH)->PlaceTile(*m_decoder, m_quant);

				// inverse transform from m_wtChannel to m_channel
				m_wtChannel[i]->InverseTransform(m_currentLevel, &m_width[i], &m_height[i], &m_channel[i]);
				ASSERT(m_channel[i]);
			}
			m_currentLevel--;
		}
	}

	/*!
	 *	\brief	Decode one level
	 *	\param	stream		Stream containing encoded data
	 */
	bool ReadLevel(CPGFStream * stream) THROW_ {
		ASSERT(m_decoder);
		int i;

		m_decoder->setStream(stream);

		// encoding scheme without ROI
		UINT8 image_id;
		m_decoder->ReadLevelHeader(image_id, levels-m_currentLevel);
		for (i=0; i < m_header.channels; i++) {
			ASSERT(m_wtChannel[i]);
			// decode file and write stream to m_wtChannel
			if (m_currentLevel == m_header.nLevels) { 
				// last level also has LL band
				m_wtChannel[i]->GetSubband(m_currentLevel, LL)->PlaceTile(*m_decoder, m_quant);
			}
			m_wtChannel[i]->GetSubband(m_currentLevel, HL)->PlaceTile(*m_decoder, m_quant);
			m_wtChannel[i]->GetSubband(m_currentLevel, LH)->PlaceTile(*m_decoder, m_quant);
			m_wtChannel[i]->GetSubband(m_currentLevel, HH)->PlaceTile(*m_decoder, m_quant);

			// inverse transform from m_wtChannel to m_channel
			m_wtChannel[i]->InverseTransform(m_currentLevel, &m_width[i], &m_height[i], &m_channel[i]);
			ASSERT(m_channel[i]);
		}

		m_currentLevel--;
		return true;
	}
};



