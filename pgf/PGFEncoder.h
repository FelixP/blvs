
#include "../common/execution_timing.h"
#include "../common/verbosity.h"

#include "mypgf.h"


class PGFEncoder {

	clock_member_decl(encode_time_sum);
	clock_member_decl(encoded_frames);

	clock_member_decl(copy_time_sum);
	clock_member_decl(copied_frames);


	PGFHeader header;
	MyPGFImage pgf;

	/// Encoded PGF
	CPGFMemoryStream stream;

	uint32_t data_size;


	static void nop(CPGFStream * stream, UINT8 level, UINT64 start_pos, UINT64 end_pos)
	{
	}

public:
	/// Callback for processing levels when they are complete
	typedef MyPGFImage::LevelEncodedCallback LevelEncodedCallback;

	struct YUYV {uint8_t y0 ,u0 ,y1 ,v0;} __attribute__((packed));
	struct YU12 {uint8_t *y ,*u ,*v;};

	PGFEncoder()
 		: stream(64000)
	{
		memset(&header, 0, sizeof(PGFHeader));
		header.bpp = 8;
		header.channels = 3;
		header.quality = 4;
		header.mode = ImageModeGrayScale;
		header.background.rgbtBlue = header.background.rgbtGreen = header.background.rgbtRed = 0;

		pgf.SetHeader(header);

		clock_member_init(encode_time_sum);
		clock_member_init(encoded_frames);

		clock_member_init(copy_time_sum);
		clock_member_init(copied_frames);
	}

	~PGFEncoder()
	{
		VERBOSITY_1(
		{
		clock_mean_msg(encode_time_sum, encoded_frames, "(PGFEncoder)   Average encoding time");
		clock_mean_msg(copy_time_sum, copied_frames,    "(PGFEncoder)   Average convert/copy to encoder time");
		});
	}

	void set_quality(int q)
	{
		ASSERT(q >= 0 && q <= 30);
		header.quality = q;
	}

	/*! from YUYV */
	PGFEncoder & put_image(YUYV * data, uint16_t width, uint16_t height)
	{
		pgf.Destroy();

		clock_start_decl(t);

		header.width = width;
		header.height = height;
		pgf.SetHeader(header);
		pgf.ImportYUYVBitmap((uint8_t *)data);

		clock_stop_sum(t, copy_time_sum);
		timing_command(copied_frames++);

		return *this;
	}


	/*! from YU12 */
	PGFEncoder & put_image(YU12 * data, uint16_t width, uint16_t height)
	{
		pgf.Destroy();

		clock_start_decl(t);

		header.width = width;
		header.height = height;
		pgf.SetHeader(header);

		MyPGFImage::YU12 d;
		d.y=data->y;
		d.u=data->u;
		d.v=data->v;
		pgf.ImportYU12toYUYVBitmap(&d);

		clock_stop_sum(t, copy_time_sum);
		timing_command(copied_frames++);

		return *this;
	}

	/*! call put_image() before */
	void encode(LevelEncodedCallback cb = &nop)
	{
		static UINT8 image_id = 0;
		clock_start_decl(t);
		
		stream.SetPos(FSFromStart, 0);
		pgf.Write(&stream, cb, image_id);
		data_size = stream.GetPos();
		clock_stop_sum(t, encode_time_sum);
		timing_command(encoded_frames++);

		VERBOSITY_4(
		{
			printf("Encoding image %i (q %d), size ", (int)image_id, (int)header.quality);
			int sum = 0;
			for (int i = pgf.Levels()-1; i >= 0; i--) {
				printf(" %d", (int)pgf.GetEncodedLevelLength(i));
				sum += pgf.GetEncodedLevelLength(i);
			}
			printf(" -> %d\n", sum);
		});
		image_id++;
	}

	/// Returns buffer of complete encoded image
	uint8_t * get_data()
	{
		return stream.GetBuffer();
	}

	/// Returns length of complete encoded image
	size_t get_size() const
	{
		return data_size;
	}
};
