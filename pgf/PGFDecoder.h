#include "../common/verbosity.h"

#include "mypgf.h"
#include "../common/hexdump.h"
#include "../common/execution_timing.h"

class PGFDecoder {

	clock_member_decl(decode_time_sum);
	clock_member_decl(decoded_frames);

	clock_member_decl(decode_time_wasted);
	clock_member_decl(decode_time_tmp);

	clock_member_decl(copy_time_sum);
	clock_member_decl(copied_frames);

	PGFHeader header;
	MyPGFImage pgf;

public:

	PGFDecoder()
	{
		// Default values
		header.bpp = 8;
		header.channels = 3;
		header.quality = 4;
		header.mode = ImageModeGrayScale;
		header.background.rgbtBlue = header.background.rgbtGreen = header.background.rgbtRed = 0;

		clock_member_init(decode_time_sum);
		clock_member_init(decoded_frames);

		clock_member_init(decode_time_wasted);
		clock_member_init(decode_time_tmp);

		clock_member_init(copy_time_sum);
		clock_member_init(copied_frames);
	}

	~PGFDecoder()
	{
		VERBOSITY_1(
		{
		clock_mean_msg(decode_time_sum, copied_frames,    "(PGFDecoder)   Average decoding time per displayed image");
		clock_mean_msg(copy_time_sum, copied_frames,      "(PGFDecoder)   Average convert/copy from decoder time");
		timing_command(fprintf(stderr,           "<timing> (PGFDecoder)   Partially decoded frames: %u\n", (unsigned int)(decoded_frames-copied_frames)));
		clock_mean_msg(decode_time_wasted, copied_frames, "(PGFDecoder)   \"Wasted\" decoding time per displayed image");
		});
	}

	/// Decode complete image
	void decode(uint8_t * data, size_t size)
	{
		clock_start_decl(t);

		CPGFMemoryStream stream(data, size);
	
		try
		{
			pgf.Destroy();
			pgf.StartRead(&stream, header);
			pgf.Read();
		}
		catch(IOException& e)
		{
			int err = e.error;
			if (err >= AppError) err -= AppError;
			fprintf(stderr, "Error: Opening and reading PGF image failed (%d)!\n", err);
			return;
		}
		
		clock_stop_sum(t, decode_time_sum);
		timing_command(decoded_frames++);
	}

	/*!
	 *	\brief	Decode one level
	 *	\param	data		Buffer containing encoded data
	 *	\param	size		Length of buffer
	 *	\param	first		true if data is first level
	 */
	void decode_level(uint8_t * data, size_t size, bool first)
	{
		clock_start_decl(t);

		CPGFMemoryStream stream(data, size);
	
		try
		{
			if (first) {
				pgf.Destroy();
				pgf.StartRead(&stream, header);
				timing_command(decoded_frames++);
				timing_command(decode_time_wasted += decode_time_tmp);
				timing_command(decode_time_tmp = 0);
			}
			pgf.ReadLevel(&stream);
		}
		catch(IOException& e)
		{
			int err = e.error;
			if (err >= AppError) err -= AppError;
			fprintf(stderr, "Error: Opening and reading PGF image failed (%d)!\n", err);
			return;
		}

		clock_stop_sum(t, decode_time_tmp);
		clock_stop_sum(t, decode_time_sum);
	}

#ifndef PGF_GIVE_YU12
	template<class T> T& give_image(T& imgstore)
	{
		clock_start_decl(t);

		UINT32 width = pgf.Width(pgf.Level());
		UINT32 height = pgf.Height(pgf.Level());
		VERBOSITY_4(printf("Display image: subscribed resolution %ux%u\n", width, height));
		
		typename T::YUYV *yuyv = ( typeof(yuyv) ) malloc(width * height * 3);
		pgf.ExportYUYV((uint8_t*)yuyv);
		
		T& ret = imgstore.put_image(yuyv, width, height);
		free(yuyv);
		
		clock_stop_sum(t, copy_time_sum);
		timing_command(copied_frames++);

		timing_command(decode_time_tmp = 0);
		
		return ret;
	}
#else
	template<class T> T& give_image(T& imgstore)
	{
		clock_start_decl(t);
// 		BYTE level = pgf.Level();
		BYTE level = 0;

		UINT32 width = pgf.Width(level);
		UINT32 height = pgf.Height(level);
		VERBOSITY_4(printf("Display image: subscribed resolution %ux%u\n", width, height));

		typename T::YU12 yu12;
		uint8_t * mem = ( typeof(mem) ) malloc(width * height * 2);
		yu12.y = mem;
		yu12.u = yu12.y + (width * height);
		yu12.v = yu12.u + ((width * height) >> 2);

		pgf.ExportYU12(mem,level);
// 		pgf.ExportYU12(mem);

		T& ret = imgstore.put_image(&yu12, width, height);
		free(mem);

		clock_stop_sum(t, copy_time_sum);
		timing_command(copied_frames++);

		timing_command(decode_time_tmp = 0);

		return ret;
	}

	template<class T> T& give_image(T& imgstore, int8_t give_level)
	{
		clock_start_decl(t);
		BYTE level = give_level;
		
		UINT32 width = pgf.Width(level);
		UINT32 height = pgf.Height(level);
		VERBOSITY_4(printf("Display image: subscribed resolution %ux%u\n", width, height));
		
		typename T::YU12 yu12;
		uint8_t * mem = ( typeof(mem) ) malloc(width * height * 2);
		yu12.y = mem;
		yu12.u = yu12.y + (width * height);
		yu12.v = yu12.u + ((width * height) >> 2);
		
		pgf.ExportYU12(mem,level);
		
		T& ret = imgstore.put_image(&yu12, width, height);
		free(mem);
		
		clock_stop_sum(t, copy_time_sum);
		timing_command(copied_frames++);
		
		timing_command(decode_time_tmp = 0);
		
		return ret;
	}
#endif
};
