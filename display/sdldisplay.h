#include <SDL/SDL.h>

#include "boost/function.hpp"

#include "../common/verbosity.h"



class SDLVideo
{
	private:
		clock_member_decl(copy_time_sum);
		clock_member_decl(copied_frames);
		clock_member_decl(copy_time_sum_lower);
		clock_member_decl(copied_frames_lower);
		
	public:
		SDL_Surface * surface;
		SDL_sem * surface_mutex;
		SDL_Rect * ovrect;
		SDL_Overlay * ov;
		struct YUYV {uint8_t y0 ,u0 ,y1 ,v0;}__attribute__((packed));
		struct YU12 {uint8_t* y ,*u ,*v; enum{uscaleh=1,uscalev=1,vscaleh=1,vscalev=1};};

		SDLVideo(SDL_Surface * surface,SDL_sem * surface_mutex,SDL_Rect * ovrect):
		surface(surface),surface_mutex(surface_mutex),ovrect(ovrect)
		{
			clock_member_init(copy_time_sum);
			clock_member_init(copied_frames);
			
			clock_member_init(copy_time_sum_lower);
			clock_member_init(copied_frames_lower);
			
			surface = SDL_GetVideoSurface();
			ov = SDL_CreateYUVOverlay(surface->w, surface->h,SDL_YV12_OVERLAY,surface);
			ovrect->x=ovrect->y=0;
			ovrect->w=surface->w;
			ovrect->h=surface->h;
		}

		~SDLVideo()
		{
			SDL_FreeYUVOverlay(ov);
			VERBOSITY_1(clock_mean_msg((copy_time_sum + copy_time_sum_lower), (copied_frames+copied_frames_lower), "(SDLVideo)\tAverage copy to SDLVideo time\t"));
		}

		void repaint()
		{
			SDL_DisplayYUVOverlay(ov, ovrect);
		}

		SDLVideo & put_image(YU12 * src,uint16_t width,uint16_t height)
		{
			clock_start_decl(t);
			
			if(SDL_SemWaitTimeout(surface_mutex, 15)==SDL_MUTEX_TIMEDOUT)
			{
				VERBOSITY_3(fprintf(stderr,"Mutex timeout reached lost Frame while Display\n"));
				return *this;
			}
			
			surface = surface?surface:SDL_GetVideoSurface();
			if(ov->w!=width||ov->h!=height)
			{
				SDL_FreeYUVOverlay(ov);
				ov = SDL_CreateYUVOverlay(width, height, SDL_YV12_OVERLAY, surface);
			}

			SDL_LockYUVOverlay(ov);
			memcpy(ov->pixels[0], src->y, width * height);
			memcpy(ov->pixels[1], src->v, (width * height) >> 2);
			memcpy(ov->pixels[2], src->u, (width * height) >> 2);
			SDL_UnlockYUVOverlay(ov);
			SDL_DisplayYUVOverlay(ov, ovrect);
			SDL_SemPost(surface_mutex);

			clock_stop_sum(t, copy_time_sum);
			timing_command(copied_frames++);
			return *this;
		
		}
		SDLVideo & put_upperimage(YU12 * src,uint16_t width,uint16_t height)
		{
			clock_start_decl(t);
			
			if(SDL_SemWaitTimeout(surface_mutex, 15)==SDL_MUTEX_TIMEDOUT)
			{
				VERBOSITY_3(fprintf(stderr,"Mutex timeout reached lost Frame while Display\n"));
				return *this;
			}
			
			surface = surface?surface:SDL_GetVideoSurface();
			if(ov->w!=width||ov->h!=height*2)
			{
				SDL_FreeYUVOverlay(ov);
				ov = SDL_CreateYUVOverlay(width, height*2, SDL_YV12_OVERLAY, surface);
			}

			SDL_LockYUVOverlay(ov);
			memcpy(ov->pixels[0], src->y, width * height);
			memcpy(ov->pixels[1], src->v, (width * height) >> 2);
			memcpy(ov->pixels[2], src->u, (width * height) >> 2);
			SDL_DisplayYUVOverlay(ov, ovrect);
			SDL_UnlockYUVOverlay(ov);
			SDL_SemPost(surface_mutex);

			clock_stop_sum(t, copy_time_sum);
			timing_command(copied_frames++);
			return *this;
		
		}
		SDLVideo & put_lowerimage(YU12 * src,uint16_t width,uint16_t height)
		{
			clock_start_decl(t);
			
			if(SDL_SemWaitTimeout(surface_mutex, 15)==SDL_MUTEX_TIMEDOUT)
			{
				VERBOSITY_3(fprintf(stderr,"Mutex timeout reached lost Frame while Display\n"));
				return *this;
			}
			surface = surface?surface:SDL_GetVideoSurface();
			if(ov->w!=width||ov->h!=height*2)
			{
				SDL_FreeYUVOverlay(ov);
				ov = SDL_CreateYUVOverlay(width, height*2, SDL_YV12_OVERLAY, surface);
			}

			SDL_LockYUVOverlay(ov);
			memcpy((ov->pixels[0] + (width * height)), src->y, width * height);
			memcpy((ov->pixels[1] + ((width * height) >> 2)), src->v, ((width * height) >> 2));
			memcpy((ov->pixels[2] + ((width * height) >> 2)), src->u, ((width * height) >> 2));
			SDL_DisplayYUVOverlay(ov, ovrect);
			SDL_UnlockYUVOverlay(ov);
			SDL_SemPost(surface_mutex);

			clock_stop_sum(t, copy_time_sum_lower);
			timing_command(copied_frames_lower++);
			return *this;
		
		}
};

class QualityBarVertical
{
	public:
		SDL_Surface * surface;
		SDL_sem * surface_mutex;
		SDL_Rect * rect;
		uint32_t fgcolor;
		uint32_t bgcolor;
		uint8_t quality;
		bool painted;
		
		QualityBarVertical(SDL_Surface * surface,SDL_sem * surface_mutex,SDL_Rect * rect, uint32_t fgcolor, uint32_t bgcolor):
		surface(surface),surface_mutex(surface_mutex),rect(rect),fgcolor(fgcolor),bgcolor(bgcolor),quality(0), painted(false)
		{}
		void repaint()
		{
			SDL_Rect qrect;
			qrect.w = rect->w;
			qrect.h = rect->h * quality / 255;
			qrect.x = rect->x;
			qrect.y = rect->y;

			SDL_FillRect(surface,rect,bgcolor);
			SDL_FillRect(surface,&qrect,fgcolor);

			if(SDL_SemWaitTimeout(surface_mutex, 15) == SDL_MUTEX_TIMEDOUT)
			{
				VERBOSITY_3(fprintf(stderr,"Mutex timeout. Quality info not displayed.\n"));
				painted = false;
				return;
			}
			SDL_UpdateRects(surface,1,rect); // Flip causes Displayflicker because it updates the whole surface and overwrites the yuyv overlay
			painted = true;
			SDL_SemPost(surface_mutex);
// 			SDL_Flip(surface);
		}
		void set_quality(uint8_t q)
		{
			if (q != quality || !painted) {
				quality = q;
				repaint();
			}
		}
};
// QualityBarHorizontal

class SDLDisplay
{

	private:
		volatile bool term;
	public:
		SDL_Surface * screen;
		SDL_sem *screen_mutex;
		SDLVideo * img;
		class UpperSDLVideo:public SDLVideo
		{
			public:
				UpperSDLVideo & put_image(YU12 * src,uint16_t width,uint16_t height)
				{
					SDLVideo::put_upperimage(src,width,height);
					return *this;
				}
		} * img1;
		class LowerSDLVideo:public SDLVideo
		{
			public:
				LowerSDLVideo & put_image(YU12 * src,uint16_t width,uint16_t height)
				{
					SDLVideo::put_lowerimage(src,width,height);
					return *this;
				}
		} * img2;
		QualityBarVertical * qb;
		SDL_Rect qb_rect;
		uint32_t surface_flags;
		SDL_Rect overlay_rect;
		
		boost::function<bool (SDL_KeyboardEvent key)> unhandeled_key_cb;
		
		SDLDisplay():term(false)
		{
			screen_mutex = SDL_CreateSemaphore(0);
			if (SDL_Init(SDL_INIT_VIDEO) < 0)
			{
				fprintf(stderr, "Couldn't init SDL : %s \n", SDL_GetError());
				exit(1);
			}

			surface_flags= SDL_HWSURFACE|SDL_ANYFORMAT|SDL_RESIZABLE|SDL_HWACCEL|SDL_DOUBLEBUF;

			screen = SDL_SetVideoMode(640, 480, 24, surface_flags);
			if (screen == NULL)
			{
				fprintf(stderr, "Couldn't set video mode : %s\n", SDL_GetError());
				exit(1);
			}
			img = new SDLVideo(screen,screen_mutex,&overlay_rect);
			
			const char * caption = "blvs-videoclient";
			SDL_WM_SetCaption(caption,caption);
			
			overlay_rect.x = 20;
			img1 = (typeof img1)img;
			img2 = (typeof img2)img;
			qb_rect.x=qb_rect.y=0;
			qb_rect.w=10;
			qb_rect.h=screen->h;
			qb = new QualityBarVertical(screen,screen_mutex, &qb_rect, SDL_MapRGB(screen->format,255,0,0), SDL_MapRGB(screen->format,0,0,0));
			
			SDL_SemPost(screen_mutex);
		}
		
		~SDLDisplay()
		{
			delete img;
			delete qb;
		}
		
		void stop()
		{
			term = true;
		}
		
		bool terminating()
		{
			return term;
		}
		
		void set_unhandeled_key_cb( boost::function<bool (SDL_KeyboardEvent key)> cb)
		{
			unhandeled_key_cb = cb;
		}
		
		// attention Posts screen_mutex
		void arrange_and_repaint()
		{
			overlay_rect.h = screen->h;
			overlay_rect.w = screen->w-20;
			qb_rect.h = screen->h;
			img->repaint();
			qb->repaint();
			SDL_SemPost(screen_mutex);
			VERBOSITY_2(printf("%i x %i \n",screen->w,screen->h));
		}
		
		void handle_key(SDL_KeyboardEvent key)
		{
			
			switch(key.keysym.sym)
			{
				case SDLK_KP_MULTIPLY:
				case SDLK_SPACE:
				case SDLK_p:
					SDL_SemWait(screen_mutex);
					screen = screen?screen:SDL_GetVideoSurface();
					{
						uint16_t w = screen->w - 20;
						screen = SDL_SetVideoMode(screen->w,(float)w/img->ov->w*img->ov->h, 24, surface_flags);
					}
					screen = SDL_GetVideoSurface();
					if ( screen == 0 )
					{
						fprintf(stderr, "Resize fehlgeschlagen: %s",SDL_GetError());
						return;
					}
					arrange_and_repaint();
					break;
				case SDLK_KP_DIVIDE:
				case SDLK_o:
					SDL_SemWait(screen_mutex);
					screen = screen?screen:SDL_GetVideoSurface();
					{
						screen = SDL_SetVideoMode(img->ov->w+20,img->ov->h, 24, surface_flags);
					}
					screen = SDL_GetVideoSurface();
					if ( screen == 0 )
					{
						fprintf(stderr, "Resize fehlgeschlagen: %s",SDL_GetError());
						return;
					}
					arrange_and_repaint();
					break;
				case SDLK_ESCAPE:
				case SDLK_q:
					stop();
					break;
				case SDLK_F1:
				case SDLK_h:
					printf("Keymapping\n");
					printf(
					"\n"
					"Key          Function\n"
					"q|esc        stop Display\n"
					"h|F1         display this help\n"
					"o|devide     resize to incoming picture size\n"
					"p|multiply   resize to incoming picture proportion\n"
					);
					(!unhandeled_key_cb || !unhandeled_key_cb(key));
					break;
				default:
					if(!unhandeled_key_cb || !unhandeled_key_cb(key))
						VERBOSITY_2(printf("unhadled Key\n"));
					break;
			}

		}

		void operator()()
		{
			SDL_Event sdl_event;
			while(!term)
			{
				SDL_WaitEvent(&sdl_event);
				SDL_SemWait(screen_mutex);
				switch(sdl_event.type)
				{
					case SDL_VIDEORESIZE:
						screen = SDL_SetVideoMode(sdl_event.resize.w,sdl_event.resize.h, 24, surface_flags);
						if ( screen == 0 )
						{
							fprintf(stderr, "Resize fehlgeschlagen: %s",SDL_GetError());
							return;
						}
						arrange_and_repaint();
						break;
					case SDL_VIDEOEXPOSE:
						qb->repaint();
						img->repaint();
						SDL_SemPost(screen_mutex);
						break;
					case SDL_KEYDOWN :
						SDL_SemPost(screen_mutex);
						handle_key(sdl_event.key);
						break;
					case SDL_QUIT:
						SDL_SemPost(screen_mutex);
						stop();
						break;
					default:
						SDL_SemPost(screen_mutex);

				}
			}

		}
};

