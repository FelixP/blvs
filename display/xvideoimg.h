#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/extensions/Xv.h>
#include <X11/extensions/Xvlib.h>
#include "../common/yuvconv.h"

#include <iostream>
#include <iomanip>

class XVideoIMG{

	private:
		clock_member_decl(copy_time_sum);
		clock_member_decl(copied_frames);


	public:
		//TODO

// 		struct RGB24 {uint8_t r,g,b;}__attribute__((packed));
		struct YUYV {uint8_t y0 ,u0 ,y1 ,v0;}__attribute__((packed));
// 		struct PGFYUV{DataT * y, * u , * v; bool undersampled;};
		typedef YUVconv::YU12 YU12;
	private:
		Display *display;
		XvImage *image;
		GC gc;
		XvPortID port;
		Colormap colourMap;
		Window window;
		XvAdaptorInfo *ai;
		XvImageFormatValues *fo;
		unsigned int image_width, image_height, window_width, window_height;
		boost::mutex windowlock;
		unsigned int format;
		bool term;

		static Atom xv_intern_atom_if_exists( Display *display, XvPortID port,char const *atom_name )
		{
			XvAttribute * attributes;
			int attrib_count,i;
			Atom xv_atom = None;

			attributes = XvQueryPortAttributes( display, port, &attrib_count );
			if( attributes!=NULL )
			{
				for ( i = 0; i < attrib_count; ++i )
				{
					if ( strcmp(attributes[i].name, atom_name ) == 0 )
					{
						xv_atom = XInternAtom( display, atom_name, False );
						break; // found what we want, break out
					}
				}
				XFree( attributes );
			}

			return xv_atom;
		}

		static Bool waitForNotify( Display *, XEvent *e, char *arg )
		{
			return ( e->type == MapNotify ) && ( e->xmap.window == (Window)arg );
		}


	public:
		XVideoIMG()
		{
			image_width = 1280, image_height = 960;
			clock_member_init(copy_time_sum);
			clock_member_init(copied_frames);
			display = XOpenDisplay( NULL );
			{
				unsigned int ver, rel, req, ev, err;
				bool retVal =( XvQueryExtension( display, &ver, &rel, &req, &ev, &err ) == Success );
				if ( !retVal )
					exit(EXIT_FAILURE);
			}
			unsigned int adaptors;
			ai = NULL;
			{
				bool retVal =( XvQueryAdaptors( display, DefaultRootWindow( display ), &adaptors,&ai ) == Success );
				if ( !retVal )
					exit(EXIT_FAILURE);
			}
			port = 0;
			for ( unsigned int i=0; i<adaptors; i++ ) {
				if ( ( ai[i].type & ( XvInputMask | XvImageMask ) ) == ( XvInputMask | XvImageMask ) )
				{
					for ( unsigned int p=ai[i].base_id; p<ai[i].base_id+ai[i].num_ports; p++ )
						if ( !XvGrabPort( display, p, CurrentTime ) )
						{
							port = p;
							break;
						};
					if ( port != 0 )
						break;
				};
			};
			if ( !port )
				exit(EXIT_FAILURE);
			#ifndef NDEBUG
			std::cerr << "Xv port is " << port << std::endl;
			#endif
			{
				int colourkey = 0;
				Atom xvColorKey = xv_intern_atom_if_exists( display, port, "XV_COLORKEY" );
				if ( xvColorKey != None )
				{
					#ifndef NDEBUG
					std::cerr << "Require drawing of colorkey" << std::endl;
					#endif
					if ( XvGetPortAttribute( display, port, xvColorKey, &colourkey ) != Success )
						exit(EXIT_FAILURE);
					Atom xvAutoPaint = xv_intern_atom_if_exists( display, port,"XV_AUTOPAINT_COLORKEY" );
					if ( xvAutoPaint != None ) {
						#ifndef NDEBUG
						std::cerr << "Enabling autopainting" << std::endl;
						#endif
						XvSetPortAttribute( display, port, xvAutoPaint, 1 );
						xvColorKey = None;
					};
				} else {
					#ifndef NDEBUG
					std::cerr << "No drawing of colourkey required" << std::endl;
					#endif
				}
			}
			{
				//duchsuche Formatliste
				unsigned int formats;
				fo = XvListImageFormats( display, port, (int *)&formats );
				if ( !fo ) exit(EXIT_FAILURE);
				format = 0;
				for ( unsigned int i=0; i<formats; i++ ) {
					#ifndef NDEBUG
					std::cerr << "found format " << (char *)&fo[i].id << " GUID 0x"
						<< std::setbase( 16 ) << fo[i].id << std::setbase( 10 ) << std::endl;
					#endif
				};
				for( unsigned int i=0; !format && i<formats; i++ )
				{
					if ( fo[i].id == (*(int*)"YUY2") )
					{
						format = fo[i].id;
						#ifndef NDEBUG
						std::cerr << "will use format " << (char *)&fo[i].id << " GUID 0x"
							<< std::setbase( 16 ) << fo[i].id << std::setbase( 10 ) << std::endl;
						#endif
					};
				};
				if ( !format ){std::cerr << "Jim"<< std::endl;; exit(EXIT_FAILURE);}
			}
			{
				int depth;
				{
					XWindowAttributes attribs;
					XGetWindowAttributes( display, DefaultRootWindow( display ), &attribs );
					depth = attribs.depth;
					if (depth != 15 && depth != 16 && depth != 24 && depth != 32) depth = 24;
				}
				XVisualInfo visualInfo;
				XMatchVisualInfo( display, DefaultScreen( display ), depth, TrueColor, &visualInfo );
				XSetWindowAttributes xswa;
				colourMap = XCreateColormap( display, DefaultRootWindow( display ), visualInfo.visual, AllocNone );
				xswa.colormap = colourMap;
				xswa.border_pixel = 0;
				xswa.event_mask = ExposureMask | StructureNotifyMask | KeyPressMask;
				unsigned long mask = CWBorderPixel | CWColormap | CWEventMask;
				window = XCreateWindow( display, RootWindow( display, visualInfo.screen ),
							0, 0, image_width, image_height, 0, visualInfo.depth, InputOutput,
							visualInfo.visual, mask, &xswa );
			}
			{
				XEvent event;
				XMapWindow( display, window );
				XIfEvent( display, &event, waitForNotify, (char *)window );

				XGCValues xgcv;
				gc = XCreateGC( display, window, 0L, &xgcv );
			}
			image = (XvImage *)XvCreateImage( display, port, format, NULL, image_width, image_height );
			// Alternatively use XvShmCreateImage and XShmAttach.
			image->data = (char *)malloc( image->data_size );
			{
				//nicht leer
				#ifndef NDEBUG
				std::cerr << "Filling image of size " << image->data_size << std::endl;
				#endif
				for ( unsigned int y=0; y<image_height; y++ )
					for ( unsigned int x=0; x<image_width; x++ )
						((unsigned short int *)image->data)[ y * image_width + x ] = y+x;
			}
			term = false;

		}



		~XVideoIMG()
		{
			stop();
			free( image->data );
			// Free shared memory!!!!
			XFree( image );
			// XSync( display, False ); ?
			XFreeGC( display, gc );
			XvUngrabPort( display, port, CurrentTime );
			XDestroyWindow( display, window );
			XFreeColormap( display, colourMap );
			XvFreeAdaptorInfo(ai);
			XFree(fo);
			XCloseDisplay( display );
			clock_mean_msg(copy_time_sum, copied_frames, "(XVideoIMG)\tAverage copy to XVideo time\t");

		}
		void stop()
		{
			term = true;
			XClientMessageEvent xevent;
			xevent.type = ClientMessage;
			XSendEvent(display, window, false, ClientMessage, (XEvent*)&xevent);

		}
		bool terminating()
		{
			return term;
		}
		XVideoIMG& put_image(YU12 * src,uint16_t width,uint16_t height)
		{
			clock_start_decl(t);

			image->width = width;
			image->height = height;
			image_width= width;
			image_height= height;

			src->width=width,src->height=height;

			YUVconv::YUYV ib(*((uint8_t*)image->data));
			ib.width=width;
			ib.height=height;
			YUVconv::convert(*src,ib);

			if( windowlock.try_lock())
			{
				XvPutImage( display, port, window, gc,image, 0, 0, image_width, image_height, 0, 0, window_width, window_height );
				windowlock.unlock();
			}

			clock_stop_sum(t, copy_time_sum);
			timing_command(copied_frames++);

			return *this;
		}

		XVideoIMG& put_image(YUYV * src,uint16_t width,uint16_t height)
		{
			clock_start_decl(t);
			
			if(term) return *this;
			image->width = width;
			image->height = height;
			image_width= width;
			image_height= height;
			
			memcpy(image->data,src,width*height*2);
			if( windowlock.try_lock())
			{
				XvPutImage( display, port, window, gc,image, 0, 0, image_width, image_height, 0, 0, window_width, window_height );
				windowlock.unlock();
			}

			clock_stop_sum(t, copy_time_sum);
			timing_command(copied_frames++);

			return *this;
		}

		void operator()()
		{
			   Atom wmDeleteMessage = XInternAtom(display, "WM_DELETE_WINDOW", False);
			   XSetWMProtocols(display, window, &wmDeleteMessage, 1);
			do {
				XEvent event;
				XNextEvent( display,  &event  );
				{
					std::cerr << "XEvent" << std::endl;
					windowlock.lock();
					switch ( event.type ) {
						case ConfigureNotify:
						case Expose:
							#ifndef NDEBUG
							std::cerr << "Repainting" << std::endl;
							#endif
							{
								unsigned int _d/*, window_width, window_height*/;
								Window _w;
								XGetGeometry(display, window, &_w, (int*)&_d, (int*)&_d, &window_width, &window_height, &_d,&_d);
								XvPutImage( display, port, window, gc,image, 0, 0, image_width, image_height, 0, 0, window_width, window_height );
								// Alternatively use XvShmPutImage.
							}
							break;
						case ClientMessage:
							if ((Atom)event.xclient.data.l[0] == wmDeleteMessage)
							{
								std::cout << "Window closed -> shutdown" << std::endl;
								term = true;
								break;
							}
							else
								break;
						case KeyPress:
							#ifndef NDEBUG
							std::cerr << "Key was pressed" << std::endl;
							#endif
							if ( event.xkey.keycode == 0x09 )
								term = true;
							break;
						default:
							std::cerr << "unknown XEvent" << std::endl;
							break;
					};
					windowlock.unlock();
				}
			} while ( !term );
		}
};
