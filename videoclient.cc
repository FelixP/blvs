// Extra keys: 1, 2, 3, 4 to toggle receiving data of the corresponding image level
#define AUTO_ADAPT_TEST

#define DO_TIMING
#define VIDEOCLIENT
#define PGF_GIVE_YU12

#include <boost/static_assert.hpp>
#include "boost/thread/mutex.hpp"
#include "boost/bind.hpp"

#include "common/verbosity.h"
#include "famouso_bindings.h"
#include "afp/config.h"
#include "afp/Defragmentation.h"
#include "afp/LargeEvent.h"

#include "common/execution_timing.h"

#include "pgf/PGFDecoder.h"

#include "display/sdldisplay.h"

// NoImageWarning headers
#include <sys/time.h>


typedef afp::frag::RedundancyAttribute<afp::DefaultTag, 0> PublishFECRedundancy;

typedef afp::AFPConfig <
	afp::ApplicationLayerDimensions,
	afp::FragUseEventSeq,
	afp::FragUseFEC,
	afp::DefragSupportEventSeq,
	afp::DefragDuplicateChecking,
	afp::DefragSupportFEC,
	PublishFECRedundancy
> MyAFPConfig;


#ifdef AUTO_ADAPT_TEST
// Auto-adaption test hack
bool __level_receive[4] = { true, true, true, true };
#endif

// Video-ID constists of 7 chars and is used to construct famouso subject names
// for publishing the video stream
const char * default_video_id = "blvs!!!";

// Default number of subscribed PGF levels (determines video resolution)
const unsigned int default_subscribed_levels = MyPGFImage::levels;


/*!
 *	\brief	Singleton class responsible for warning the user if last displayed video frame gets old
 */
class NoImageWarning {

	SDLDisplay * video_display;

	struct timeval last_display_time;

	struct itimerval interval_timer_config;

	/// Return instance of singleton class
	static NoImageWarning & inst()
	{
		static NoImageWarning niw;
		return niw;
	}

public:
	/// Timer (SIGALRM) handler called if we displayed no new frame for a certain time
	static void no_image_warning(int sig)
	{
		// No display activity anymore... update warning bar
		struct timeval now, diff;
		gettimeofday(&now, NULL);
		timersub(&now, &inst().last_display_time, &diff);

		int t = ((diff.tv_sec*1000000 + diff.tv_usec) - 110000) / 10000;
		if (t > 255)
			t = 255;
		else if (t < 0)
			t = 0;

		inst().video_display->qb->set_quality(t);
	}

	/// Inform this class about a video frame got displayed
	static void image_displayed()
	{
		// Reset timer (calls no_image_warning())
		setitimer(ITIMER_REAL, &inst().interval_timer_config, NULL);

		// Save last display time
		gettimeofday(&inst().last_display_time, NULL);

		// Reset warning bar
		inst().video_display->qb->set_quality(0);
	}

	/// Init this subsystem
	static void init(SDLDisplay & disp)
	{
		inst().video_display = &disp;

		inst().interval_timer_config.it_value.tv_sec = 0;
		inst().interval_timer_config.it_value.tv_usec = 110000;	// First timer call after 0.11 sec
		inst().interval_timer_config.it_interval.tv_sec = 0;
		inst().interval_timer_config.it_interval.tv_usec = 50000;	// Next call the timer every 0.05 sec

		signal(SIGALRM, &no_image_warning);
	}
};

class VideoStream  {

	SDLDisplay & sd;
	PGFDecoder pgf_dec;

	/// Mutex for avoiding race conditions of multiple concurrent callbacks (every subscribed channel has it's own thread)
	boost::mutex mutex;

	/// ID (Sequence number) of the video frame we currently receive. -1 before anything arrived.
	uint8_t curr_image_id;
	
	/// True if next image is first image (or first after a long blackout time thus needing resetting curr_image_id)
	bool first_image;

	/// True after an image was displayed. Indicates that a new image to decode can be chosen.
	bool image_complete;

	/// Number of image levels of current image already decoded
	unsigned int decoded_levels;

	/// Bitmask containing information which image levels already arrived for which Image-IDs
	uint8_t arrived_levels_mask[256];

	/// Number of subscribed levels
	unsigned int subscribed_levels;
	
	/// Number of used levels (smaller or equal to subsrived_levels, smaller only if auto-adaption is turned on)
	unsigned int used_levels;

	/// Mask to match for all used levels having been arrived
	unsigned int used_levels_mask;

	/// Video-ID
	const char * video_id;

	/// Dualcam-Mode (0 = no dualcam, 1 = upper image, 2 = lower image)
	int dualcam_mode;

	/// Subscriber event channel to use
	typedef famouso::config::SEC SEC;


	/// Auto adaption of displayed resolution data (level counters & sum)
	int auto_adaption[MyPGFImage::levels];

	/// Auto adaption: reset data (init & change of subscribed levels)
	void auto_adaption_reset()
	{
		for (int i = 0; i < MyPGFImage::levels; i++)
			auto_adaption[i] = 0;
	}

	/// Auto adaption: inform about received level and auto-adapt
	void auto_adaption_received(unsigned int level)
	{
		// Increase counters
		assert(level < MyPGFImage::levels);
		auto_adaption[level]++;

		// Auto-adaption
		if (auto_adaption[0] >= 2) {

			unsigned int new_levels = used_levels;

			assert(4 == MyPGFImage::levels);
			if (auto_adaption[1] == 0) {
				new_levels = 1;
			} else if (auto_adaption[2] == 0) {
				new_levels = 2;
			} else if (auto_adaption[3] == 0) {
				new_levels = 3;
			} else {
				new_levels = 4;
			}

			if (new_levels != used_levels) {
				VERBOSITY_1(printf("autoadapt: level counter: %d %d %d %d -> changed resolution from level %u to %u\n", auto_adaption[0], auto_adaption[1], auto_adaption[2], auto_adaption[3], used_levels, new_levels));
				used_levels = new_levels;
				used_levels_mask = (1 << new_levels) - 1;
			} else {
				VERBOSITY_3(
				static int i = 0;
				if (i % 40 == 0)
					printf("autoadapt: level counter: %d %d %d %d\n", auto_adaption[0], auto_adaption[1], auto_adaption[2], auto_adaption[3]);
				i++;
				);
			}

			auto_adaption_reset();
		}
	}


	/*!
	 *	\brief	Special AFP subscriber event channel. One instance per level.
	 */
	class Channel : public SEC {

		/// AFP config to use
		typedef MyAFPConfig AFPRC;

		/// Level this channel is assigned to
		const unsigned int level;

		const uint8_t level_mask;
		
		const uint8_t all_lower_levels_mask;

		/// Defragmenter handles for all image-IDs
		void * decode_handles[256];

		/// Index in decode_handler to start next cleaning of outdated data with
		uint8_t decode_outdated_marker;

		/// Use AFP defragmentation processor that support later delivering of messages if they cannot be processed yet.
		afp::DefragmentationProcessorKeepEventSupport<AFPRC> defrag_processor;

		/// VideoStream instance this Channel belongs to
		VideoStream * video_stream;

		/// True if channel is about do be deleted -> stop any activity
		volatile bool in_destruction;

		/// Get subject from video_id and channel_id
		static const char * get_subject(const char * video_id, char channel_id)
		{
			static char s [8];
			memcpy(&s, video_id, 7);
			s[7] = channel_id;
			return s;
		}

		/// Check whether received_id is outdated in terms of current_id
		static bool is_outdated(uint8_t current_id, uint8_t received_id)
		{
			uint8_t later_than_current = received_id - current_id;
			uint8_t earlier_than_current = 0-later_than_current;
			//printf("current: %i, received %i, later_than_current: %i earlier_than_current: %i\n", curr_id, image_id, later_than_current, earlier_than_current);
			return later_than_current > earlier_than_current;
		}

		/// Check whether videoserver was restarted (received_id is very different from current_id)
		static bool is_server_restarted(uint8_t current_id, uint8_t received_id)
		{
			uint8_t later_than_current = received_id - current_id;
			uint8_t earlier_than_current = 0-later_than_current;
			//printf("current: %i, received %i, later_than_current: %i earlier_than_current: %i\n", curr_id, image_id, later_than_current, earlier_than_current);
			uint8_t diff = (later_than_current < earlier_than_current ? later_than_current : earlier_than_current);
			return diff > 10;	// abs. difference between image_ids more than 10 -> server was restarted
		}

		/// Save level data for decoding of image image_id
		void ready_to_decode(uint8_t image_id, afp::DefragmentationStep<AFPRC> & ds)
		{
			decode_handles[image_id] = defrag_processor.keep_event(ds);
		}

		/// Free all outdated kept defragmenter handles of this level (images)
		void clean_outdated_data()
		{
			uint8_t curr = video_stream->curr_image_id;
			for(uint8_t c = decode_outdated_marker; c != curr; c++) { // overflow wanted
				if (decode_handles[c]) {
//					printf("Free image %u level %i\n", (unsigned int)c, (int)level);
					defrag_processor.kept_event_processed(decode_handles[c]);
					decode_handles[c] = 0;
					video_stream->arrived_levels_mask[c] = 0;
				}
			}
			decode_outdated_marker = curr;
		}

		/*!
		 *	\brief	Decode this and above levels as far as they already arrived. Call this only for level 0.
		 */
		void decode()
		{
			uint8_t image_id = video_stream->curr_image_id;

			// Cancel decoding if this level did not arrive yet or channel is in destruction
			if ((video_stream->arrived_levels_mask[image_id] & level_mask) == 0 || in_destruction)
				return;

			// Decode this level if it has not been decoded yet
			if (video_stream->decoded_levels == level) {
//				printf("Decoding image %u level %i\n", (unsigned int) image_id, (int)level);
				void * handle = decode_handles[image_id];
				uint8_t * data = defrag_processor.kept_get_event_data(handle);
				uint32_t length = defrag_processor.kept_get_event_length(handle);

				video_stream->pgf_dec.decode_level(data, length, level == 0);
				video_stream->decoded_levels++;
			}

			// Display image if we decoded as much levels as we want
			if (level + 1 == video_stream->used_levels) {
				// Image complete... display
				VERBOSITY_5(printf("Display image_id %i\n", (int)image_id));
				switch (video_stream->dualcam_mode) {
					case 1:  video_stream->pgf_dec.give_image(*(video_stream->sd.img1), (MyPGFImage::levels - video_stream->subscribed_levels)); break;
					case 2:  video_stream->pgf_dec.give_image(*(video_stream->sd.img2), (MyPGFImage::levels - video_stream->subscribed_levels)); break;
					default: video_stream->pgf_dec.give_image(*(video_stream->sd.img), (MyPGFImage::levels - video_stream->subscribed_levels)); break;
				}
				video_stream->image_complete = true;
				video_stream->arrived_levels_mask[image_id] = 0;
				NoImageWarning::image_displayed();
			} else { 
				// Decode higher levels
				video_stream->channel[level + 1]->decode();
			}

			// Free data of outdated frames (for this level)
			clean_outdated_data();
		}

	public:
		
		/*!
		 *	\brief	Constructs a new level subscriber channel
		 *	\param	video_stream	Parent video stream
		 *	\param	level		Image level that will be received at this channel (0 to subscribed levels - 1)
		 *	\param	video_id	Video-ID (7 Byte) for construction subject
		 *	\param	channel_id	Channel-ID (1 Byte) for construction subject
		 */
		Channel(VideoStream * video_stream, unsigned int level, const char * video_id, char channel_id)
			: SEC(get_subject(video_id, channel_id)),
			  level(level),
			  level_mask(1 << level),
			  all_lower_levels_mask(level_mask - 1),
			  decode_outdated_marker(0),
			  defrag_processor(1024),					// TODO: MTU bestimmen!
			  video_stream(video_stream),
			  in_destruction(false)
		{
			memset(decode_handles, 0, sizeof(decode_handles));
			callback.bind< typeof(*this), & Channel::incoming_data_callback >(this);
			subscribe();
		}

		/*!
		 *	\brief	Destruct level subscriber channel
		 *	\pre	Thread owns video_stream->mutex
		 */
		~Channel()
		{
			// Workaround to avoid cleanup conflicts with subscriber callback thread:
			// - no thread can be in deep in callback, because this is called while video_stream->mutex is hold
			// - ensure that threads waiting vor video stream mutex will leave callback
			in_destruction = true;
			// - unlock mutex to let thread leave callback
			video_stream->mutex.unlock();
			// - unsubscribe to ensure callback is not called again
			unsubscribe();
			// - restore previous mutex state
			video_stream->mutex.lock();

			// Free kept events not used
			for(int c = 0; c <= 255; c++) {
				if (decode_handles[c]) {
					defrag_processor.kept_event_processed(decode_handles[c]);
				}
			}
		}

		/*!
		 *	\brief	Called if data arrives through this channel
		 */
		void incoming_data_callback(famouso::mw::api::SECCallBackData & cbd)
		{
#ifdef AUTO_ADAPT_TEST
			if (!__level_receive[level])
				return;
#endif

			// AFP defragmentation
			afp::DefragmentationStep<AFPRC> ds(cbd.data, cbd.length);
			defrag_processor.process_fragment(ds);

			// Only continue if an event is complete
			if (!ds.event_complete())
				return;

			afp::LargeEvent e(cbd.subject);
			e.data = ds.get_event_data();
			e.length = ds.get_event_length();

			uint8_t image_id = CDecoder::PreviewLevelHeader(e.data, level == 0);

			// Lock video_stream to prevent race conditions from concurrent callbacks (on different channels)
			video_stream->mutex.lock();

			if (in_destruction)
				goto unlock_and_leave_callback;


			// Inform auto adaption mechanism about received level
			video_stream->auto_adaption_received(level);

			// If this level is not needed, drop it
			if (level >= video_stream->used_levels) {
				defrag_processor.event_processed(ds);
				goto unlock_and_leave_callback;
			}


			if (video_stream->first_image || is_server_restarted(video_stream->curr_image_id, image_id)) {
				// First image after server (re)start
				VERBOSITY_1(printf("Server (re)started: expected image = %i; received image = %i\n", (int)video_stream->curr_image_id, (int)image_id));
				video_stream->curr_image_id = image_id;
				video_stream->image_complete = true;
				video_stream->first_image = false;
			}

			if (is_outdated(video_stream->curr_image_id, image_id)) {
				// Drop outdated image level
				video_stream->arrived_levels_mask[image_id] = 0;
				VERBOSITY_5(printf("Dropping outdated image %i level %i. Current image: %i\n", (int)image_id, (int)level, (int)video_stream->curr_image_id));
				defrag_processor.event_processed(ds);
				goto unlock_and_leave_callback;
			}

			VERBOSITY_5(printf("Receiving image %i level %i (size %u). Current image: %i\n", (int)CDecoder::PreviewLevelHeader(e.data, level==0), level, e.length, (int)video_stream->curr_image_id));

			// Save which levels alread arrived for this image
			video_stream->arrived_levels_mask[image_id] |= level_mask;

			if (video_stream->image_complete) {
				// Last image complete and displayed
				// Start new frame: take image we just received
				video_stream->curr_image_id = image_id;
				video_stream->decoded_levels = 0;
				video_stream->image_complete = false;
				VERBOSITY_5(printf("Start decoding image %i\n", (int)CDecoder::PreviewLevelHeader(e.data, level==0)));
			}

			if (video_stream->arrived_levels_mask[image_id] == video_stream->used_levels_mask && video_stream->curr_image_id != image_id) {
				// All levels of newer image arrived... drop old frame
				VERBOSITY_4(printf("Skip incomplete outdated frame to display newer frame which is already complete\n"));
				// Start new frame
				video_stream->curr_image_id = image_id;
				video_stream->decoded_levels = 0;
				video_stream->image_complete = false;
				VERBOSITY_5(printf("Start decoding image %i\n", (int)CDecoder::PreviewLevelHeader(e.data, level==0)));
			}

			// Provide data for decoding
			ready_to_decode(image_id, ds);

			// Start decoding chain
			video_stream->channel[0]->decode();

unlock_and_leave_callback:
			video_stream->mutex.unlock();
			return;
		}
	};

	/// Array of possible level channels (first subscribed_levels entries are valid)
	Channel * channel[MyPGFImage::levels];


public:
	/*!
	 *	\brief	Construct video stream
	 *	\param	sdl	SDLDisplay to use
	 *	\param	levels	Initial number of levels to subscribe
	 *	\param	video_id	Video-ID (7 Byte) of the video stream)
	 *	\param	cam	0 = single cam mode, 1 = this is upper video stream, 2 = this is lower video stream
	 */
	VideoStream(SDLDisplay & sdl, int levels, const char * video_id, int cam = 0)
		: sd(sdl), curr_image_id(0), first_image(true), image_complete(true), decoded_levels(0),
		  subscribed_levels(0), video_id(video_id), dualcam_mode(cam)
	{
		memset(arrived_levels_mask, 0, 256);
		
		auto_adaption_reset();
		set_subscribed_levels(levels);
	}
	
	/*!
	 *	\brief	Destructor
	 */
	~VideoStream()
	{
		set_subscribed_levels(0);
	}
	
	/*!
	 *	\brief	Change number of subscribed levels (change video stream resolution)
	 *	\param	levels	Range: 0 to MyPGFImage::levels; 0 = no video, 1 = lowest resolution
	 */
	bool set_subscribed_levels(int levels)
	{
		if (levels < 0 || levels > MyPGFImage::levels)
			return false;

		mutex.lock();

		if ((int)subscribed_levels > levels) {
			// Unsubscribe levels
			int old_sl = subscribed_levels;
			subscribed_levels = levels;
			used_levels = levels;
			used_levels_mask = (1 << levels) - 1;
			for (int l = old_sl - 1; l >= levels; l--) {
				delete channel[l];
			}
		} else if ((int)subscribed_levels < levels) {
			// Subscribe levels
			for (int l = subscribed_levels; l < levels; l++) {
				channel[l] = new Channel(this, l, video_id, '0' + l);
			}
			subscribed_levels = levels;
			used_levels = levels;
			used_levels_mask = (1 << levels) - 1;
		}

		// Reset auto-adaption data
		auto_adaption_reset();

		mutex.unlock();

		return true;
	}
	
	/*!
	 *	\brief	Return number of subscribed levels
	 */
	int get_subscribed_levels()
	{
		return subscribed_levels;
	}
};


void print_statistics()
{
	afp::defrag::FragmentStats fs;
	afp::defrag::Statistics<afp::DefaultTag>::get_fragment_stats(fs);

	afp::defrag::EventStats es;
	afp::defrag::Statistics<afp::DefaultTag>::get_event_stats(es);

	printf("AFP STATS: Fragments: received %u, used %u", fs.received, fs.used);
	if (fs.incorrect_header)
		printf(", incorrect header %u", fs.incorrect_header);
	if (fs.outdated)
		printf(", outdated %u", fs.outdated);
	if (fs.duplicates)
		printf(", duplicates %u", fs.duplicates);
	printf("; expected %u\n", fs.expected);

	printf("AFP STATS: Events: complete %u, incomplete %u\n", es.complete, es.incomplete);
}


/*!
 *	\brief	Handle SDL key event not handles by SDLDisplay itself
 *	\param	key	Keyboard event
 *	\param	vs1	First video stream (may be NULL)
 *	\param	vs2	Second video stream (may be NULL)
 */
bool handle_key(SDL_KeyboardEvent key, VideoStream * vs1, VideoStream * vs2)
{
	switch(key.keysym.sym)
	{
		case SDLK_a:
			print_statistics();
			if(key.keysym.mod & KMOD_SHIFT)
				afp::defrag::Statistics<afp::DefaultTag>::reset();
			return true;
#ifdef AUTO_ADAPT_TEST
		case SDLK_1:
			__level_receive[0] = !__level_receive[0];
			return true;
		case SDLK_2:
			__level_receive[1] = !__level_receive[1];
			return true;
		case SDLK_3:
			__level_receive[2] = !__level_receive[2];
			return true;
		case SDLK_4:
			__level_receive[3] = !__level_receive[3];
			return true;
#endif
		case SDLK_PLUS:
		case SDLK_KP_PLUS:
			if (vs1) {
				vs1->set_subscribed_levels(vs1->get_subscribed_levels()+1);
			}
			if (vs2) {
				vs2->set_subscribed_levels(vs2->get_subscribed_levels()+1);
			}
			return true;
		case SDLK_MINUS:
		case SDLK_KP_MINUS:
			if (vs1) {
				vs1->set_subscribed_levels(vs1->get_subscribed_levels() > 1 ? vs1->get_subscribed_levels() - 1 : 1);
			}
			if (vs2) {
				vs2->set_subscribed_levels(vs2->get_subscribed_levels() > 1 ? vs2->get_subscribed_levels() - 1 : 1);
			}
			return true;
		case SDLK_F1:
		case SDLK_h:
			printf(
			"\n"
			"a            display AFP-Statistics\n"
			"Shift + a    display and reset AFP-Statistics\n"
			"+            raise subscribed PGF-levels by one\n"
			"-            reduce subscribed PGF-levels by one\n"
			"\n"
			);
		default:
			return false;
	}
}


/*!
 *	\brief	Entry point
 */
int main(int argc, char **argv)
{
	const char * video_id = default_video_id;
	const char * video_id2 = 0;
	unsigned int levels = default_subscribed_levels;

	bool wrong_args = false;
	int arg = 1;
	enum { other_arg, id_arg, levels_arg, dual_arg, verbosity_arg } next_arg = other_arg; 

	while (!wrong_args && arg < argc) {
		if (argv[arg][0] == '-') {
			if (strcmp(argv[arg] + 1, "-id") == 0)
				next_arg = id_arg;
			else if (strcmp(argv[arg] + 1, "-verbosity") == 0)
				next_arg = verbosity_arg;
			else if (strcmp(argv[arg] + 1, "v") == 0)
				next_arg = verbosity_arg;
			else if (strcmp(argv[arg] + 1, "-levels") == 0)
				next_arg = levels_arg;
			else if (strcmp(argv[arg] + 1, "-dual") == 0)
				next_arg = dual_arg;
			else
				wrong_args = true;
		} else {
			if (next_arg == id_arg) {
				video_id = argv[arg];
				int len = strlen(video_id);
				if (len != 7) {
					fprintf(stderr, "Error: Length of Video-ID must be 7 byte.\n");
					wrong_args = true;
				}
			} else if (next_arg == dual_arg) {
				video_id2 = argv[arg];
				int len = strlen(video_id2);
				if (len != 7) {
					fprintf(stderr, "Error: Length of Video-ID must be 7 byte.\n");
					wrong_args = true;
				}
			} else if (next_arg == verbosity_arg) {
				if(SET_VERBOSITY(atoi(argv[arg]))){
					fprintf(stderr, "Error: Verbosity level not supported.\n");
					wrong_args = true;
				}
			} else if (next_arg == levels_arg) {
				levels = atoi(argv[arg]);
				if (levels <= 0 || levels > MyPGFImage::levels) {
					fprintf(stderr, "Error: Number of levels has to be between 1 and %i.\n", MyPGFImage::levels);
					wrong_args = true;
				}
			} else {
				wrong_args = true;
			}
			next_arg = other_arg;
		}
		arg++;
	}

	if (wrong_args || next_arg != other_arg) {
		fprintf(stderr, 
			"Usage:\n"
			"    videoclient [OPTIONS]\n"
			"\n"
			"  OPTIONS:\n"
			"    --id VIDEO_ID   Set ID used for publishing the video stream via\n"
			"                    the famouso middleware. It has to be a string of\n"
			"                    7 bytes. Default Video-ID: %s\n"
			"   --levels LEV     Subscribe LEV levels. Choose 1 for minimum video\n"
			"                    resolution and %i for maximum. Default is %i\n"
			"   --dual VIDEO_ID  Run in dualcam mode. VIDEO_ID is the ID of the\n"
			"                    second video stream; see --id option for details.\n"
			"   --verbosity LEVEL Verbositylevel (0 - 5)\n"
			"   -v LEVEL         Verbositylevel (0 - 5)\n"
			"\n"
			, default_video_id
			, MyPGFImage::levels
			, default_subscribed_levels
		);
		return -1;
	}

	famouso::init<famouso::config>();
	SDLDisplay display;

	NoImageWarning::init(display);

	if (video_id2) {
		// Dualcam mode
		VideoStream vs1(display, levels, video_id, 1);
		VideoStream vs2(display, levels, video_id2, 2);
		display.unhandeled_key_cb = boost::bind(handle_key, _1, &vs1, &vs2);
		display();
	} else {
		// Singlecam mode
		VideoStream vs(display, levels, video_id);
		display.unhandeled_key_cb = boost::bind(handle_key, _1, &vs, (VideoStream *)0);
		display();
	}
	
	return 0;
}

