DEBUG = -g -O3
#DEBUG = -DNDEBUG -O3

EXTERNALS_PATH ?= externals
include $(EXTERNALS_PATH)/externalsmakehelp.mk

CFLAGS = -Wall -lpthread  $(DEBUG)

###############################################################################
#  Rules  #####################################################################

SOURCES=$(wildcard *.cc)
BINS=$(SOURCES:.cc=)

XVCFLAGS =  -L/usr/X11R6/lib -lX11 -lXext -lXv
SDLCFLAGS =  -lSDL

LIBS-videoserver = $(FAMOUSO) $(PGF) $(V4L2)
LIBS-videoclient = $(FAMOUSO) $(PGF) $(CIMG) $(SDLCFLAGS)


.PHONY: all externals doc clean


all: $(BINS)

externals:
	make -C $(EXTERNALS_PATH) getall

doc:
	doxygen doc/doxygen.conf

$(BINS): % : %.cc $(PGF_DEP)
	$(CXX) -o $@ $@.cc $(CFLAGS) $(LIBS-$@)

clean:
	rm -f $(BINS)
