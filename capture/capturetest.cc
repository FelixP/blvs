#define DO_TIMING

// #define V4L2C_USE_READ
// #define V4L2C_USE_YU12
// #define NO_LIBV4L2
// #define V4L2C_CLOSE_TO_RATE

// #define SW
#define YUYV2RGB

#define doPGF

#include "../common/execution_timing.h"

#include "v4l2capture.h"

#include "../common/CImgIM.h"

#include "../common/FileImage.h"

#include <iostream>
#include <iomanip>

#include "boost/thread/thread.hpp"
#include "boost/thread/mutex.hpp"

#ifdef doPGF
#include <iostream>
using namespace std;
#include "../pgf/PGFEncoder.h"
#include "../pgf/PGFDecoder.h"
#endif

// #include "../display/xvideoimg.h"
#include "../display/sdldisplay.h"

class Capture
{
	V4l2ImgStream * Stream1;

	clock_member_decl(frame_to_frame_time_sum);
	clock_member_decl(frames);

	public:

		Capture(int vd)
		{
			clock_member_init(frame_to_frame_time_sum);
			clock_member_init(frames);
			Stream1 = new V4l2ImgStream(vd);
		}

		~Capture()
		{
			clock_mean_msg(frame_to_frame_time_sum, frames, "(Capture)\tDurchschnittsinterframezeit\t");
			delete Stream1;
		}

		
		void shpic()
		{
// 			CImgIM CPic;
			#ifdef doPGF
			PGFEncoder PGFenc;
			PGFenc.set_quality(6);
			PGFDecoder PGFdec;
			#endif
// 			Stream1->give_image(CPic);
// 			cimg_library::CImgDisplay disp(CPic);
// 			CImgDisplayThread tdisp(640,480);
// 			boost::thread dispthrd(boost::ref(tdisp));

// 			XVideoIMG xv;
// 			boost::thread dispthrd(boost::ref(xv));
			SDLDisplay sd;
			boost::thread dispthrd(boost::ref(sd));
			clock_decl(frame_time);
			clock_start(frame_time);
			{
//				while(!disp.is_closed)
// 				while(!xv.terminating())
// 				while(!tdisp.terminating())
				while(!sd.terminating())
				{
					#ifndef doPGF
					{
// 						Stream1->give_image(CPic);
						
// 						Stream1->give_image(xv);
						Stream1->give_image(*sd.img);
			
// 						char filename[20];
// 						sprintf(filename, "v/frame-%06d.yuyv", frame_no++);
// 						FileImage fi(filename);
// 						Stream1->give_image(fi);
					}
					#else
					{
						Stream1->give_image(PGFenc);
						PGFenc.encode();
						uint8_t* buffer = PGFenc.get_data();
						size_t sbuffer = PGFenc.get_size();
						PGFdec.decode(buffer,sbuffer);
						PGFdec.give_image(*sd.img);
					}
					#endif
//					tdisp.putpic(CPic);
// 					CPic.display(disp);
					
					clock_stop_sum(frame_time,frame_to_frame_time_sum);clock_start(frame_time);
					timing_command(frames++);
					
// 					printf("frame_time: %i\n",Stream1->get_frame_time_d());
				}
			}
			dispthrd.join();
		}
		
		void operator()()
		{
			Stream1->set_frame_wait_time_ms(100);
			
			if(Stream1->set_capture_format(640,480) && Stream1->start_streaming())
			{
				shpic();
				Stream1->stop();
			}
		}
};

int main (int argc, char **argv)
{
	int vd = open(argv[1],O_RDWR);
	Capture c(vd);
	c();
	sleep(1);
}
