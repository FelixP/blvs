#include <errno.h>
#include <assert.h>
#include <poll.h>
#include <stdint.h>
#include <string.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/ioctl.h>

#include "../common/verbosity.h"
#include "../common/execution_timing.h"
#include "boost/thread/thread.hpp"
#include "boost/interprocess/sync/interprocess_semaphore.hpp"
#include "boost/thread/shared_mutex.hpp"
#include "boost/thread/mutex.hpp"
#include "boost/bind.hpp"

#include <linux/videodev2.h>

#ifndef NO_LIBV4L2
#include <libv4l2.h>
#else
#define v4l2_fd_open(fd,flag) fd
#define v4l2_ioctl ioctl
#define v4l2_open open
#define v4l2_mmap mmap
#define v4l2_munmap munmap
#define v4l2_read read
#endif

namespace guard
{
	//copy from boost added sharedlock_guard
	template<typename Mutex> class lock_guard
	{
		private:
			Mutex& m;

			explicit lock_guard(lock_guard&);
			lock_guard& operator=(lock_guard&);
		public:
			explicit lock_guard(Mutex& m_):
			m(m_)
			{
				m.lock();
			}
			~lock_guard()
			{
				m.unlock();
			}
	};

	template<typename Mutex> class sharedlock_guard
	{
		private:
			Mutex& m;

			explicit sharedlock_guard(sharedlock_guard&);
			sharedlock_guard& operator=(sharedlock_guard&);
		public:
			explicit sharedlock_guard(Mutex& m_):
			m(m_)
			{
				m.lock_shared();
			}
			~sharedlock_guard()
			{
				m.unlock_shared();
			}
	};
	
}

#define lock_guard(mutex) guard::lock_guard<typeof(mutex)> __mutex__guard(mutex)
#define sharedlock_guard(mutex) guard::sharedlock_guard<typeof(mutex)> 	__mutex__guard(mutex)



class V4l2ImgStream
{
	enum{  numbuffer = 2};
	int vd;
	v4l2_format v4l2_pix;
#ifndef V4L2C_USE_READ
	boost::interprocess::interprocess_semaphore pollsem;
	boost::interprocess::interprocess_semaphore buffersem;
	boost::mutex bufferslock;
	boost::thread framerate_thread;
#endif
	boost::shared_mutex configurationlock;
	
	volatile bool term;
	volatile bool stream_stoped;
	int frame_wait_time_ms;
	uint frame_time_d_us;
	uint last_frame_time_us;
	
	clock_member_decl(polllock_time_sum);
	clock_member_decl(bufferlock_time_sum);
	clock_member_decl(dq_time_sum);
	clock_member_decl(copy_time_sum);
	clock_member_decl(frames);
	
	struct 
	{
		v4l2_buffer * start;
		size_t length;
		volatile bool free;
	}buffers[numbuffer];

	static inline unsigned int get_time_us()
	{
		struct timeval t;
		gettimeofday(&t, 0);
		return t.tv_sec * 1000 * 1000 + t.tv_usec ;	// time in us
	}
	
	public:
		V4l2ImgStream(int fd)
		#ifndef V4L2C_USE_READ
			:pollsem(0),buffersem(0)
		#endif
		{
			vd = v4l2_fd_open(fd,0);
			frame_wait_time_ms = 100;
			term = false;
			stream_stoped=true;
			frame_time_d_us = 0 ;
			last_frame_time_us = 0;
			
			clock_member_init(polllock_time_sum);
			clock_member_init(bufferlock_time_sum);
			clock_member_init(dq_time_sum);
			clock_member_init(copy_time_sum);
			clock_member_init(frames);
			
			v4l2_capability v4l2_cap;
			memset(&v4l2_cap,0,sizeof(v4l2_cap));
			int ret = v4l2_ioctl(vd,VIDIOC_QUERYCAP, &v4l2_cap);
			if (ret < 0) {
				fprintf(stderr, "Error: unable to query device.\n");
				exit(1);
			}
			if (!(v4l2_cap.capabilities && V4L2_CAP_VIDEO_CAPTURE))
			{
				fprintf(stderr, "Error: is not capable to capture.\n");
				exit(1);
			}
			
			ret = 0;

			VERBOSITY_2(
			{
			printf("(Lib)V4L2 support the following capture Format on Device\n");
			for(int i = 0;ret==0;i++){
				
				v4l2_fmtdesc fmt;
				memset(&fmt,0,sizeof(fmt));
				fmt.type=V4L2_BUF_TYPE_VIDEO_CAPTURE;
				fmt.index = i;
				ret = v4l2_ioctl(vd,VIDIOC_ENUM_FMT,&fmt);
				printf("%.4s",(uint8_t*)&fmt.pixelformat);
				printf(" fmt %i :%x\n",fmt.index,fmt.pixelformat);
			}
			});
			//set_capture_format(320,240);
			set_capture_format(640,480);
			//set_capture_format(1280,960);
		}
		~V4l2ImgStream()
		{
			stop();
			usleep(100000);
			VERBOSITY_1(
			{
			timing_command(fprintf(stderr, "<timing> (V4l2ImgStream)\tcaptured %i Frames\n",frames);)
			clock_mean_msg(polllock_time_sum,frames,"(V4l2ImgStream)\tdurchschnittliche Pollwartezeit \t");
			clock_mean_msg(bufferlock_time_sum,frames,"(V4l2ImgStream)\tdurchschnittliche Bufferlockzeit \t");
			clock_mean_msg(dq_time_sum,frames,"(V4l2ImgStream)\tdurchschnittliche Dequeuezeit \t");
			clock_mean_msg(copy_time_sum,frames,"(V4l2ImgStream)\tdurchschnittliche Kopierzeit \t");
			});
		}
		bool set_capture_format(int width,int height)
		{
			lock_guard(configurationlock);
			memset(&v4l2_pix,0,sizeof(v4l2_pix));
			
			v4l2_pix.type=V4L2_BUF_TYPE_VIDEO_CAPTURE;
		#ifdef V4L2C_USE_YU12
			v4l2_pix.fmt.pix.pixelformat = V4L2_PIX_FMT_YUV420;
		#else
			v4l2_pix.fmt.pix.pixelformat = V4L2_PIX_FMT_YUYV;
		#endif
// 			v4l2_pix.fmt.pix.pixelformat = V4L2_PIX_FMT_RGB24;

			v4l2_pix.fmt.pix.width=width;
			v4l2_pix.fmt.pix.height=height;
			VERBOSITY_2(
			{
			printf("Setup V4L2 capture format\n");
			printf("Try to get:\t%.4s @ %i x %i\n",(uint8_t*)&v4l2_pix.fmt.pix.pixelformat,v4l2_pix.fmt.pix.width,v4l2_pix.fmt.pix.height);
			});
			int ret = v4l2_ioctl(vd,VIDIOC_S_FMT,&v4l2_pix);
			if (ret < 0) {
				fprintf(stderr, "Error VIDIOC_S_FMT %i \n",ret);
			}
			VERBOSITY_2(
			{
			printf("Will get:\t%.4s @ %i x %i\n",(uint8_t*)&v4l2_pix.fmt.pix.pixelformat,v4l2_pix.fmt.pix.width,v4l2_pix.fmt.pix.height);
			});

			VERBOSITY_1(
			{
			ret = ioctl(vd,VIDIOC_G_FMT,&v4l2_pix);
			if (ret < 0) {
				fprintf(stderr, "Error VIDIOC_G_FMT %i \n",ret);
			}
			printf("Device set to:\t%.4s @ %i x %i\n",(uint8_t*)&v4l2_pix.fmt.pix.pixelformat,v4l2_pix.fmt.pix.width,v4l2_pix.fmt.pix.height);
			ret = v4l2_ioctl(vd,VIDIOC_G_FMT,&v4l2_pix);
			if (ret < 0) {
				fprintf(stderr, "Error VIDIOC_G_FMT %i \n",ret);
			}
			});
			return !(ret < 0);
		}
		
		void stop()
		{
			term = true;
			stop_streaming();
		}
		bool terminating()
		{
			return term;
		}
		bool streaming_stoped()
		{
			return stream_stoped;
		}

		int get_image_size()
		{
			return v4l2_pix.fmt.pix.sizeimage;
		}
		bool start_streaming()
		{
		#ifndef V4L2C_USE_READ
			lock_guard(configurationlock);
			if(stream_stoped == false) return false;
			bufferslock.lock();
			struct v4l2_requestbuffers reqbuf;
			
			memset (&reqbuf, 0, sizeof (reqbuf));
			reqbuf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
			reqbuf.memory = V4L2_MEMORY_MMAP;
			reqbuf.count = numbuffer;
			
			if (-1 == v4l2_ioctl (vd, VIDIOC_REQBUFS, &reqbuf)) {
				if (errno == EINVAL)
					fprintf (stderr,"Video capturing or mmap-streaming is not supported\n");
				else
					perror ("VIDIOC_REQBUFS");
				
				exit (EXIT_FAILURE);
			}
			
			if (reqbuf.count != numbuffer) 
			{
				fprintf (stderr,"Not enough buffer memory\n");
				exit (EXIT_FAILURE);
			}
			
			memset (buffers,0,sizeof (buffers));
			assert (buffers != NULL);
			
			for (unsigned int i = 0; i < reqbuf.count; i++) {
				struct v4l2_buffer buffer;
				
				memset (&buffer, 0, sizeof (buffer));
				buffer.type = reqbuf.type;
				buffer.memory = V4L2_MEMORY_MMAP;
				buffer.index = i;
				
				if (-1 == v4l2_ioctl (vd, VIDIOC_QUERYBUF, &buffer)) {
					perror ("VIDIOC_QUERYBUF");
					exit (EXIT_FAILURE);
				}
				
				buffers[i].length = buffer.length; /* remember for munmap() */
				
				buffers[i].start = (v4l2_buffer *) (v4l2_mmap (NULL, buffer.length,PROT_READ | PROT_WRITE, MAP_SHARED,vd, buffer.m.offset));
				buffers[i].free=true;
				if (MAP_FAILED == buffers[i].start) {
					/* If you do not exit here you should unmap() and free()
					the buffers mapped so far. */
					perror ("mmap");
					exit (EXIT_FAILURE);
				}
			}
			
			
			enum v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
			if (-1 == v4l2_ioctl(vd,VIDIOC_STREAMON, &type))
			{
				perror ("VIDIOC_STREAMON");
				exit (EXIT_FAILURE);
			}
			printf("Streaming\n");
			buffersem.post();
			buffersem.post();
			bufferslock.unlock();
			clock_member_init(bufferlock_time_sum);
			stream_stoped = false;
			framerate_thread = boost::thread(boost::ref(*this));
			return true;
		#else
			lock_guard(configurationlock);
			if(stream_stoped == false) return false;
			stream_stoped = false;
			return true;
		#endif
			
		}
		bool stop_streaming()
		{
		#ifndef V4L2C_USE_READ
			if(stream_stoped == true) return false;
			stream_stoped=true;
			buffersem.post();
			pollsem.post();
			framerate_thread.join();
			lock_guard(configurationlock);
			while(buffersem.try_wait());
			while(pollsem.try_wait());
			
			enum v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
			if (-1 == v4l2_ioctl(vd,VIDIOC_STREAMOFF, &type))
			{
				perror ("VIDIOC_STREAMOFF");
				exit (EXIT_FAILURE);
			}
			#if 1
			if (-1 == v4l2_ioctl(vd,VIDIOC_STREAMON, &type))
			{
				perror ("VIDIOC_STREAMON");
				exit (EXIT_FAILURE);
			}
			if (-1 == v4l2_ioctl(vd,VIDIOC_STREAMOFF, &type))
			{
				perror ("VIDIOC_STREAMOFF");
				exit (EXIT_FAILURE);
			}
			#endif
			bufferslock.lock();
			for (unsigned int i = 0; i < numbuffer; i++)
			{
				buffers[i].free=false;
				if (v4l2_munmap(buffers[i].start,buffers[i].length))
					perror ("munmap");
			}
			bufferslock.unlock();
			return true;
		#else
			if(stream_stoped == true) return false;
			stream_stoped=true;
			return true;
		#endif
		}

		bool enque_buffer()
		{
		#ifndef V4L2C_USE_READ
			sharedlock_guard(configurationlock);
			for(int i=0 ;i<numbuffer;i++)
			{
				if (buffers[i].free)
				{
					bufferslock.lock();
					struct v4l2_buffer buf;
					buffers[i].free=false;
					memset (&buf, i, sizeof (buf));
					buf.type        = V4L2_BUF_TYPE_VIDEO_CAPTURE;
					buf.memory      = V4L2_MEMORY_MMAP;
					buf.index       = i;
					if (-1 == v4l2_ioctl (vd,VIDIOC_QBUF,&buf))
					{
						perror ("VIDIOC_QBUF");
						exit (EXIT_FAILURE);
					}
					bufferslock.unlock();
					return true;
				}
			}
			return false;
		#else
			return true;
		#endif
		}
		
		
		template<class T> T& give_image(T& imgstore)
		{
			sharedlock_guard(configurationlock);
			if(term || stream_stoped) return imgstore;
	#ifdef V4L2C_USE_READ
			//Do not use this
			clock_start_decl(t);
			
			uint8_t * pic = (typeof(pic)) malloc(v4l2_pix.fmt.pix.sizeimage);
			assert (pic != NULL);
			v4l2_read(vd, pic, v4l2_pix.fmt.pix.sizeimage);
			
			clock_stop_sum(t,dq_time_sum);
			
			uint time=get_time_us();
			if(last_frame_time_us != 0)
			{
				uint d_last_frame_time_us = time - last_frame_time_us;
// 				frame_time_d_us = d_last_frame_time_us;
				frame_time_d_us = frame_time_d_us==0?d_last_frame_time_us:(frame_time_d_us+d_last_frame_time_us)>>1;
// 				frame_time_d_us = frame_time_d_us==0?d_last_frame_time_us:(frame_time_d_us>1+d_last_frame_time_us+d_last_frame_time_us)>>1
			}
			last_frame_time_us = time;
			
			clock_start(t);
			
		#ifdef V4L2C_USE_YU12 //begin USEYU12
			T* ret_tmp;
			{
				typename T::YU12 bu;
				bu.y=(uint8_t*)pic;
				bu.u=(uint8_t*)pic+(v4l2_pix.fmt.pix.width*v4l2_pix.fmt.pix.height);
				bu.v=(uint8_t*)pic+(v4l2_pix.fmt.pix.width*v4l2_pix.fmt.pix.height)+(v4l2_pix.fmt.pix.width/2*v4l2_pix.fmt.pix.height/2);
				ret_tmp = &imgstore.put_image(&bu, v4l2_pix.fmt.pix.width, v4l2_pix.fmt.pix.height);
			}
			T& ret = *ret_tmp;
		#else //end USEYU12
			T& ret = imgstore.put_image(( typename T::YUYV *) pic,v4l2_pix.fmt.pix.width,v4l2_pix.fmt.pix.height);
		#endif
// 	 		T& ret = imgstore.put_image(( typename T::RGB24 *) pic,v4l2_pix.fmt.pix.width,v4l2_pix.fmt.pix.height);
			
			pic = (typeof(pic))realloc(pic,0);
			
			clock_stop_sum(t,copy_time_sum);
			
			timing_command(frames++);
			return ret;
	#else
			
			if(term || stream_stoped) return imgstore;
			clock_start_decl(t);
			{	
				//wait for buffer being avail
				pollsem.wait();
				pollfd p;
				do{
// 					std::cerr << "falsch aktive wait" << std::endl;
					memset (&p,0,sizeof (p));
					p.fd=vd;
					p.events=POLLIN;
					if(term || stream_stoped) return imgstore;
					if (-1 == poll(&p,1,-1))
						perror ("POLL");
				}while(p.revents & POLLERR);
				
			}
			clock_stop_sum(t,polllock_time_sum);

			uint time=get_time_us();
			if(last_frame_time_us != 0)
			{
				uint d_last_frame_time_us = time - last_frame_time_us;
// 				frame_time_d_us = d_last_frame_time_us;
				frame_time_d_us = frame_time_d_us==0?d_last_frame_time_us:(frame_time_d_us+d_last_frame_time_us)>>1;
// 				frame_time_d_us = frame_time_d_us==0?d_last_frame_time_us:(frame_time_d_us>1+d_last_frame_time_us+d_last_frame_time_us)>>1
			}
			last_frame_time_us = time;
			
			struct v4l2_buffer buf;
			clock_start(t);
			
			memset (&buf, 0, sizeof (buf));
			buf.type        = V4L2_BUF_TYPE_VIDEO_CAPTURE;
			buf.memory      = V4L2_MEMORY_MMAP;
			if (-1 == v4l2_ioctl (vd,VIDIOC_DQBUF,&buf))
			{
				perror ("VIDIOC_DQBUF");
				exit (EXIT_FAILURE);
			}
			
			clock_stop_sum(t,dq_time_sum);
			
			clock_start(t);
		#ifdef V4L2C_USE_YU12 //begin USEYU12
			T* ret_tmp;
			{
				typename T::YU12 bu;
				bu.y=(uint8_t*)buffers[buf.index].start;
				bu.u=(uint8_t*)buffers[buf.index].start+(v4l2_pix.fmt.pix.width*v4l2_pix.fmt.pix.height);
				bu.v=(uint8_t*)buffers[buf.index].start+(v4l2_pix.fmt.pix.width*v4l2_pix.fmt.pix.height)+(v4l2_pix.fmt.pix.width/2*v4l2_pix.fmt.pix.height/2);
				
				ret_tmp = &imgstore.put_image(&bu, v4l2_pix.fmt.pix.width, v4l2_pix.fmt.pix.height);
			}
			T& ret = *ret_tmp;
		#else //end USEYU12
			T& ret = imgstore.put_image(( typename T::YUYV *) buffers[buf.index].start, v4l2_pix.fmt.pix.width, v4l2_pix.fmt.pix.height);
		#endif //end USEYU12

// 			T& ret = imgstore.put_image(( typename T::RGB24 *) buffers[buf.index].start, v4l2_pix.fmt.pix.width, v4l2_pix.fmt.pix.height);
// 			T& ret = imgstore.put_image(buffers[buf.index].start, v4l2_pix.fmt.pix.width, v4l2_pix.fmt.pix.height);
			
			clock_stop_sum(t,copy_time_sum);
			timing_command(frames++);
			bufferslock.lock();
			buffers[buf.index].free = true;
			bufferslock.unlock();
			buffersem.post();
			return ret;
	#endif
		}
		
		void set_frame_wait_time_ms(int frame_wait_time_ms)
		{
			this->frame_wait_time_ms=frame_wait_time_ms;
		}
		int get_frame_wait()
		{
			return frame_wait_time_ms;
		}
		int get_frame_time_d()
		{
			return frame_time_d_us;
		}
#ifndef V4L2C_USE_READ
	#ifndef V4L2C_NO_FREE_BUFFER
		void free_buffer()
		{
			static boost::mutex mx;
			if(!mx.try_lock()) return;
			VERBOSITY_3(fprintf(stderr,"free_buffer()\n"));
			{
				//wait for buffer being avail
				pollsem.wait();
				pollfd p;
				do{
// 					std::cerr << "falsch aktive wait" << std::endl;
					memset (&p,0,sizeof (p));
					p.fd=vd;
					p.events=POLLIN;
					if(term || stream_stoped) return;
					if (-1 == poll(&p,1,-1))
						perror ("POLL");
				}while(p.revents & POLLERR);

			}

			struct v4l2_buffer buf;
			memset (&buf, 0, sizeof (buf));
			buf.type        = V4L2_BUF_TYPE_VIDEO_CAPTURE;
			buf.memory      = V4L2_MEMORY_MMAP;
			if (-1 == v4l2_ioctl (vd,VIDIOC_DQBUF,&buf))
			{
				perror ("VIDIOC_DQBUF");
				exit (EXIT_FAILURE);
			}

			bufferslock.lock();
			buffers[buf.index].free = true;
			bufferslock.unlock();
			buffersem.post();
			mx.unlock();
		}
	#endif
#endif
		
	#ifndef V4L2C_USE_READ
		void operator()()
		{
			
		#ifndef V4L2C_CLOSE_TO_RATE
			boost::posix_time::ptime next_time= boost::get_system_time();
		#endif
			while(!term && !stream_stoped)
			{
				clock_start_decl(t);
		#ifndef V4L2C_NO_FREE_BUFFER
				if(!buffersem.try_wait())
				{
					boost::thread(boost::bind(&V4l2ImgStream::free_buffer,this)).detach();
					buffersem.wait();
				}
		#else
				buffersem.wait();
		#endif
				if(!enque_buffer())
				{
					fprintf(stderr,"falschq\n");
					clock_stop_sum(t,bufferlock_time_sum);
				}
				else
				{
// 					printf("q\n");
					pollsem.post();
					clock_stop_sum(t,bufferlock_time_sum);
				#ifndef V4L2C_CLOSE_TO_RATE
					if( frame_wait_time_ms > 10)
					{
						next_time += boost::posix_time::milliseconds(frame_wait_time_ms);
						while (next_time <= boost::get_system_time())
							next_time += boost::posix_time::milliseconds(frame_wait_time_ms);
						boost::this_thread::sleep(next_time);
					}
				#else
					boost::this_thread::sleep(boost::posix_time::milliseconds(frame_wait_time_ms));
				#endif
				}
			}
		}
	#endif
		
};
// class V4l2DevCtrl