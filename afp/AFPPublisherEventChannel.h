/*******************************************************************************
*
* Copyright (c) 2009 Philipp Werner <philipp.werner@st.ovgu.de>
* All rights reserved.
*
*    Redistribution and use in source and binary forms, with or without
*    modification, are permitted provided that the following conditions
*    are met:
*
*    * Redistributions of source code must retain the above copyright
*      notice, this list of conditions and the following disclaimer.
*
*    * Redistributions in binary form must reproduce the above copyright
*      notice, this list of conditions and the following disclaimer in
*      the documentation and/or other materials provided with the
*      distribution.
*
*    * Neither the name of the copyright holders nor the names of
*      contributors may be used to endorse or promote products derived
*      from this software without specific prior written permission.
*
*
*    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
*    IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
*    TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
*    PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*    OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*    SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*    LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*
* $Id: $
*
******************************************************************************/


#include "config.h"
#include "Fragmenter.h"
#include "LargeEvent.h"

#include "mw/common/Event.h"


namespace afp {

using famouso::mw::Event;
using famouso::mw::Subject;



/*!
 *	\brief	Publisher Event Channel with fragmentation support in the application layer
 *	\param	PEC	Publisher Event Channel
 *	\param	AFPSC	AFP config
 */
template <class PEC, class AFPSC = afp::ApplicationLayerDefaultConfig>
class AFPPublisherEventChannel : public PEC  {

	/// Maximum fragment size
	int mtu;

	/// Fragment buffer
	uint8_t * buffer;

public:

	/*!
	 *	\brief	Constructor
	 *	\param	s	Subject of the channel
	 *	\param	mtu	MTU to use for fragmentation. Should be same for all publisher/subscriber of this subject.
	 */
	AFPPublisherEventChannel(const Subject &s, uint16_t mtu = 1024)
		: PEC(s), mtu(mtu)
	{
		buffer = new uint8_t [mtu];
	}

	/*!
	 *	\brief	Destructor
	 */
	~AFPPublisherEventChannel()
	{
		if (buffer)
			delete [] buffer;
	}
	
	/*!
	 *	\brief	Publish an event
	 */
	void publish(const Event & e)
	{
		publish(e.data, e.length, e.subject);
	}

	/*!
	 *	\brief	Publish a large event (length > 2^16)
	 */
	void publish(const LargeEvent & e)
	{
		publish(e.data, e.length, e.subject);
	}

	/*!
	 *	\brief	Publish an event
	 *	\param	data	Event data to publish
	 *	\param	length	Length of event data
	 *	\param	subject	Subject of the event
	 */
	void publish(uint8_t * data, uint32_t length, const famouso::mw::Subject & subject)
	{
		// TODO: only use AFP if fragmentation is necessary
		Event fragment_e(subject);
		fragment_e.data = buffer;
	
		if (length != (typename AFPSC::elen_t) length) {
			std::cerr << "AFP ERROR: Cannot publish event... too large." << std::endl;
			return;
		}

		afp::Fragmenter<AFPSC> f(data, length, mtu);

		if (f.error())
			return;
				
		while ( (fragment_e.length = f.get_fragment(fragment_e.data)) ) {
			PEC::publish(fragment_e);
		}
	}

};

}

