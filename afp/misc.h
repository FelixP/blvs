

#ifndef __MISC_H_2FDBCC14EE6AA7__
#define __MISC_H_2FDBCC14EE6AA7__

/*!
 *	\brief	Integer division rounding up
 *	\param	x	dividend
 *	\param	y	divisor
 *	\returns	quotient rounded up
 */
template <typename T> static inline T div_round_up(T x, T y)
{
	// TODO: formal check
	if (x > 0)
		return (((x - 1) / y) + 1);
	else
		return 0;
}


/// Cast to data type with double count of bits
static inline uint16_t doublebits_cast(uint8_t n)
{
	return n;
}

/// Cast to data type with double count of bits
static inline uint32_t doublebits_cast(uint16_t n)
{
	return n;
}

/// Cast to data type with double count of bits
static inline uint64_t doublebits_cast(uint32_t n)
{
	return n;
}


#endif // __MISC_H_2FDBCC14EE6AA7__

