/*******************************************************************************
*
* Copyright (c) 2009 Philipp Werner <philipp.werner@st.ovgu.de>
* All rights reserved.
*
*    Redistribution and use in source and binary forms, with or without
*    modification, are permitted provided that the following conditions
*    are met:
*
*    * Redistributions of source code must retain the above copyright
*      notice, this list of conditions and the following disclaimer.
*
*    * Redistributions in binary form must reproduce the above copyright
*      notice, this list of conditions and the following disclaimer in
*      the documentation and/or other materials provided with the
*      distribution.
*
*    * Neither the name of the copyright holders nor the names of
*      contributors may be used to endorse or promote products derived
*      from this software without specific prior written permission.
*
*
*    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
*    IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
*    TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
*    PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*    OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*    SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*    LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*
* $Id: $
*
******************************************************************************/


#ifndef __ENDIAN_H_A2C4D446B0FC52__
#define __ENDIAN_H_A2C4D446B0FC52__


#include "util/endianness.h"


namespace afp {

namespace common {

namespace endian {


/*!
 *	\brief	Convert network byte order to host byte order.
 */
static inline uint8_t n2h(uint8_t n)
{
	return n;
}

/*!
 *	\brief	Convert network byte order to host byte order.
 */
static inline uint16_t n2h(uint16_t n)
{
	return ntohs(n);
}

/*!
 *	\brief	Convert network byte order to host byte order.
 */
static inline uint32_t n2h(uint32_t n)
{
	return ntohl(n);
}

/*!
 *	\brief	Convert host byte order to network byte order.
 */
static inline uint8_t h2n(uint8_t n)
{
	return n;
}

/*!
 *	\brief	Convert host byte order to network byte order.
 */
static inline uint16_t h2n(uint16_t n)
{
	return htons(n);
}

/*!
 *	\brief	Convert host byte order to network byte order.
 */
static inline uint32_t h2n(uint32_t n)
{
	return htonl(n);
}


} // namespace endian

} // namespace common

} // namespace afp


#endif // __ENDIAN_H_A2C4D446B0FC52__

