/*******************************************************************************
*
* Copyright (c) 2009 Philipp Werner <philipp.werner@st.ovgu.de>
* All rights reserved.
*
*    Redistribution and use in source and binary forms, with or without
*    modification, are permitted provided that the following conditions
*    are met:
*
*    * Redistributions of source code must retain the above copyright
*      notice, this list of conditions and the following disclaimer.
*
*    * Redistributions in binary form must reproduce the above copyright
*      notice, this list of conditions and the following disclaimer in
*      the documentation and/or other materials provided with the
*      distribution.
*
*    * Neither the name of the copyright holders nor the names of
*      contributors may be used to endorse or promote products derived
*      from this software without specific prior written permission.
*
*
*    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
*    IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
*    TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
*    PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*    OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*    SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*    LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*
* $Id: $
*
******************************************************************************/

#ifndef __STATISTICS_H_2F8C97C9EC89D7__
#define __STATISTICS_H_2F8C97C9EC89D7__


#include <string.h>

#include "boost/thread/mutex.hpp"


namespace afp {

namespace defrag {


struct FragmentStats {
	unsigned int expected;

	unsigned int received;
	unsigned int incorrect_header;
	unsigned int outdated;
	unsigned int used;
	unsigned int duplicates;

	FragmentStats()
	{
		reset();
	}

	void reset()
	{
		memset(this, 0, sizeof(*this));
	}
};

struct EventStats {
	unsigned int complete;
	unsigned int incomplete;

	EventStats()
	{
		reset();
	}

	void reset()
	{
		memset(this, 0, sizeof(*this));
	}
};


template <class Tag>
class Statistics {

	static FragmentStats & fragment()
	{
		static FragmentStats s;
		return s;
	}

	static EventStats & event()
	{
		static EventStats s;
		return s;
	}

	static boost::mutex & mutex()
	{
		static boost::mutex m;
		return m;
	}

	static void inc(unsigned int & val)
	{
		mutex().lock();
		val++;
		mutex().unlock();
	}

	static void add(unsigned int & val, unsigned int to_add)
	{
		mutex().lock();
		val += to_add;
		mutex().unlock();
	}

public:

	static void reset()
	{
		mutex().lock();
		fragment().reset();
		event().reset();
		mutex().unlock();
	}

	/// Increment total number of fragments received (called by DefragmentationProcessor)
	static void fragment_received() { inc(fragment().received); }

	/// Increment number of fragments with incorrect header (called by DefragmentationProcessor)
	static void fragment_incorrect_header() { inc(fragment().incorrect_header); }

	/// Increment total number of fragments ariving too late (called by DefragmentationProcessor)
	static void fragment_outdated() { inc(fragment().outdated); }

	/// Increment number of duplicate fragments (called by Defragmenter)
	static void fragment_duplicate() { inc(fragment().duplicates); }

	/// Increment number of used fragments (called by data reconstruction policy)
	static void fragment_used() { inc(fragment().used); }

	/// Increase number of expected fragments (called by data reconstruction policy)
	static void fragments_expected(unsigned int n) { add(fragment().expected, n); }


	/// Increment number of reconstructed events (called by data reconstruction policy)
	static void event_complete()   { inc(event().complete); }

	/// Increment number of events not completely reconstructed (called by data reconstruction policy)
	static void event_incomplete() { inc(event().incomplete); }


	static void get_fragment_stats(FragmentStats & fs)
	{
		mutex().lock();
		memcpy(&fs, &fragment(), sizeof(fs));
		mutex().unlock();
	}

	static void get_event_stats(EventStats & es)
	{
		mutex().lock();
		memcpy(&es, &event(), sizeof(es));
		mutex().unlock();
	}
};



class NoStatistics {
public:

	static void reset()
	{
	}

	/// Increment total number of fragments received (called by DefragmentationProcessor)
	static void fragment_received() {}

	/// Increment number of fragments with incorrect header (called by DefragmentationProcessor)
	static void fragment_incorrect_header() {}

	/// Increment total number of fragments ariving too late (called by DefragmentationProcessor)
	static void fragment_outdated() {}

	/// Increment number of duplicate fragments (called by Defragmenter)
	static void fragment_duplicate() {}

	/// Increment number of used fragments (called by data reconstruction policy)
	static void fragment_used() {}

	/// Increase number of lost fragments (called by data reconstruction policy)
	static void fragments_lost(unsigned int n) {}


	/// Increment number of reconstructed events (called by data reconstruction policy)
	static void event_complete()   {}

	/// Increment number of events not completely reconstructed (called by data reconstruction policy)
	static void event_incomplete() {}


	static void get_fragment_stats(FragmentStats & fs)
	{
	}

	static void get_event_stats(EventStats & es)
	{
	}
};



} // namespace defrag

} // namespace afp


#endif // __STATISTICS_H_2F8C97C9EC89D7__

