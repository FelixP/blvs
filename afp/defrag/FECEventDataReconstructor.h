/*******************************************************************************
*
* Copyright (c) 2009 Philipp Werner <philipp.werner@st.ovgu.de>
* All rights reserved.
*
*    Redistribution and use in source and binary forms, with or without
*    modification, are permitted provided that the following conditions
*    are met:
*
*    * Redistributions of source code must retain the above copyright
*      notice, this list of conditions and the following disclaimer.
*
*    * Redistributions in binary form must reproduce the above copyright
*      notice, this list of conditions and the following disclaimer in
*      the documentation and/or other materials provided with the
*      distribution.
*
*    * Neither the name of the copyright holders nor the names of
*      contributors may be used to endorse or promote products derived
*      from this software without specific prior written permission.
*
*
*    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
*    IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
*    TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
*    PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*    OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*    SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*    LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*
* $Id: $
*
******************************************************************************/


#ifndef __FECEVENTDATARECONSTRUCTOR_H_3E331BC5510F68__
#define __FECEVENTDATARECONSTRUCTOR_H_3E331BC5510F68__


#include <assert.h>

#include "../misc.h"


namespace afp {

namespace defrag {


namespace vandermonde_fec {
extern "C" {
// Forward error correction library (packet erasure code based on vandermonde matrices)
#include "../../externals/vdmfec/fec.h"
}
}


/*!
 *	\brief	Policy: Reconstruct event data from fragments supporting
 *		forward error correction.
 *
 *	If you use this policy, FECHeaderSupport should be the FEC header
 *	policy of your choice.
 *
 *	Alternative policies: NoFECEventDataReconstructor
 */
template <class AFPRC>
class FECEventDataReconstructor {

	typedef typename AFPRC::elen_t elen_t;
	typedef typename AFPRC::flen_t flen_t;
	typedef typename AFPRC::fcount_t fcount_t;

	typedef class AFPRC::DefragStatistics Statistics;

private:

	uint8_t * output_data;	///< Restored data block
	elen_t output_length;	///< Length of the currently restored data (sum of put fragment's length)

	uint8_t ** input_chunks;///< Array of k input data chunks (copied)
	int * chunk_order;	///< Array (k elements): chunk_order[n'th_put_chunk] = m'th chunk of data
				///<                                         block (reconstruction order)
	fcount_t col_chunks;	///< Count of data chunks collected.

	flen_t max_chunk_length;///< Maximum chunk length without header

	fcount_t k;		///< Count of non-redundancy data chunks
	fcount_t n;		///< Count of all data chunks

	void * fec_code;	///< Code descriptor of the used library

        /// Calculates n-k from k and redundancy level (in %)
	static fcount_t get_redundancy_fragment_count(fcount_t k, uint8_t redundancy)
	{
		return div_round_up(doublebits_cast(k * redundancy),
		                    doublebits_cast((fcount_t)100));
	}

public:

	/*!
	 *	\brief	Constructor
	 *	\param	payload_length	Maximum payload length per fragment
	 */
	FECEventDataReconstructor(flen_t payload_length)
		: output_data(0), input_chunks(0), col_chunks(0), max_chunk_length(payload_length), k(1), fec_code(0)
	{
	}

	/// Destructor
	~FECEventDataReconstructor()
	{
		if (get_data() == 0)
			Statistics::event_incomplete();

		if (fec_code) {
			vandermonde_fec::fec_free(fec_code);

			for (fcount_t i = 0; i < k; i++)
				if (input_chunks[i])
					delete [] input_chunks[i];
			delete [] input_chunks;
			delete [] chunk_order;
		}
		if (output_data) {
			delete [] output_data;
		}
	}

	/*!
	 *	\brief	Process new fragment
	 *	\param	header	AFP header of the fragment
	 *	\param	data	Payload data of the fragment
	 *	\param	length	Payload data length of the fragment
	 */
	void put_fragment(const Headers<AFPRC> & header, const uint8_t * data, flen_t length)
	{
		// TODO: support without FEC
		assert(header.fec.occurs());	// Cannot handle events using no FEC
		assert(max_chunk_length != 0);

		if (!input_chunks) {
			// First chunk... init parameters from header
			k = header.fec.get_k();
			n = k + get_redundancy_fragment_count(k, header.fec.get_red());
			output_length = (k-1) * max_chunk_length + header.fec.get_len_rest();

			Statistics::fragments_expected(n);

			fec_code = vandermonde_fec::fec_new(k, n);
			assert(fec_code);

			input_chunks = new uint8_t * [k];
			memset(input_chunks, 0, k * sizeof(uint8_t *));
			chunk_order = new int [k];

// 			printf("AFP: FEC Code: chunk_length %d, k %d, n %d\n", (int)max_chunk_length, (int)k, (int)n);
		}

		assert(!get_data());		// Event should not be already complete
		assert(col_chunks < k);

		// Collect data
		input_chunks[col_chunks] = new uint8_t [max_chunk_length];
		memcpy(input_chunks[col_chunks], data, length);
		if (max_chunk_length > length)
			memset(input_chunks[col_chunks] + length, 0, max_chunk_length - length);

		fcount_t upcount_seq = n - header.fseq - 1;
		chunk_order[col_chunks] = upcount_seq;

		col_chunks++;
		/*
		if (header.first_fragment)
			chunk_count_final = true;

		if (!col_data) {
			// put_data_fragment() called first time
			chunk_count = header.fseq + (header.first_fragment ? 1 : 2);
			col_data = new uint8_t [chunk_count * max_chunk_length];
		}

		if (header.fseq >= chunk_count) {
			// Current chunk  much too late. It has to be placed somewhere in front of
			// all data chunks received till now (fragment_count_final != true).
			realloc_col_data(header.fseq + 1);
		}

		// Chunk fits into col_data
		memcpy(col_data + (chunk_count - header.fseq - 1) * max_chunk_length, data, length);

		col_chunks++;
		col_length += length;
*/
		Statistics::fragment_used();

		if (get_data() != 0)
			Statistics::event_complete();
	}

private:
	/*!
	 *	\brief Prepending realloc (add memory in front)
	 */
/*	void realloc_col_data(fcount_t new_chunk_count)
	{
		elen_t old_len = chunk_count * max_chunk_length;
		elen_t new_len = new_chunk_count * max_chunk_length;

		uint8_t * new_col_data = new uint8_t [new_len];
		memcpy(new_col_data + (new_len - old_len), col_data, old_len);
		delete [] col_data;

		col_data = new_col_data;
		chunk_count = new_chunk_count;
	}
*/
public:
	/*!
	 *	\brief	Returns the defragmented data block if event is complete, NULL otherwise.
	 */
	uint8_t * get_data()
	{
		// If we have less than k fragments, reconstruction is not possible (yet).
		if (col_chunks < k)
			return 0;

		if (!output_data) {
			// Decode data
			int r = vandermonde_fec::fec_decode(fec_code,
			                                    input_chunks,
			                                    chunk_order,
			                                    max_chunk_length);
			assert(r == 0);

			// Copy to one output buffer
			output_data = new uint8_t [output_length];
			uint8_t * p = output_data;
			fcount_t km1 =  k-1;
			for (fcount_t i = 0; i < km1; i++, p += max_chunk_length)
				memcpy(p, input_chunks[i], max_chunk_length);
			memcpy(p, input_chunks[km1], output_length - (km1 * max_chunk_length));
		}
		return output_data;
	}

	/*!
	 *	\brief	Returns the length of the event's data. Only valid if get_data() != 0.
	 */
	elen_t get_length()
	{
		return output_length;
	}
};


} // namespace defrag

} // namespace afp


#endif // __FECEVENTDATARECONSTRUCTOR_H_3E331BC5510F68__

