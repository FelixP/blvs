/*******************************************************************************
*
* Copyright (c) 2009 Philipp Werner <philipp.werner@st.ovgu.de>
* All rights reserved.
*
*    Redistribution and use in source and binary forms, with or without
*    modification, are permitted provided that the following conditions
*    are met:
*
*    * Redistributions of source code must retain the above copyright
*      notice, this list of conditions and the following disclaimer.
*
*    * Redistributions in binary form must reproduce the above copyright
*      notice, this list of conditions and the following disclaimer in
*      the documentation and/or other materials provided with the
*      distribution.
*
*    * Neither the name of the copyright holders nor the names of
*      contributors may be used to endorse or promote products derived
*      from this software without specific prior written permission.
*
*
*    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
*    IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
*    TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
*    PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*    OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*    SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*    LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*
* $Id: $
*
******************************************************************************/


#ifndef __NOEVENTSEQDEMUX_H_068FBC9538BBE3__
#define __NOEVENTSEQDEMUX_H_068FBC9538BBE3__


#include "Defragmenter.h"


namespace afp {

namespace defrag {


/*!
 *	\brief	Policy: Event demultiplexer not supporting event sequence numbers
 *
 *	IMPORTANT: This demux policy cannot handle multiple arriving events at one time.
 *	Thus multithreading will cause problems if threads share one DefragmentationProcessor!
 *	You also have to ensure that a received event's data is not needed anymore when
 *	next event's fragments are going to be processed!
 *
 *	Use this policy for channels without packet loss, reordering and late duplicates.
 *	This policy is a little robust concerning packet loss, but may lead to erroneous
 *	events if first fragment is lost.
 *
 *	If you use this policy, NoEventSeqHeaderSupport should be the event sequence number
 *	policy of your choice.
 *
 *	Alternatives: EventSeqDemux
 */
template <class AFPRC>
class  NoEventSeqDemux {
	
	typedef typename AFPRC::elen_t   elen_t;
	typedef typename AFPRC::flen_t   flen_t;
	typedef typename AFPRC::fcount_t fcount_t;
	
protected:
// TODO: Konstruktor/Destruktor-Aufruf mit seLBST DEFINIERTEN new/delete

	/// MTU of fragments (maximum size including header)
	flen_t mtu;
	
private:
	/// Defragmenter of currently received event
	Defragmenter<AFPRC> * d;
	
public:

	/// Constructor
	NoEventSeqDemux(flen_t mtu)
		: mtu(mtu), d(0)
	{
	}
	
	/// Destructor
	~NoEventSeqDemux()
	{
		if (d)
			delete d;
	}

	void * get_defragmenter_handle(const Headers<AFPRC> & header /* TODO: Absender-Adresse, Subject? */)
	{
		// Make it more robust.
		// Channel is assumed to be perfect, so old defragmenters should be freed when
		// a new first fragment arrives. But in case there are packet loss or reordering
		// drop incomplete event and start new defragmentation.
		if (header.first_fragment && d) {
			free_defragmenter(d);
		}
	
		if (!d) {
			assert(mtu > header.length());
			d = new Defragmenter<AFPRC>(mtu - header.length());
		}

		// TODO: really necessary??
		if (d->get_event_data()) {
			// Event already complete... drop fragment
			return 0;
		}
		
		return d;
	}
	
	Defragmenter<AFPRC> * get_defragmenter(void * handle)
	{
		return d;
	}
	
	void free_defragmenter(void * handle)
	{
		delete d;
		d = 0;
	}
	
};


} // namespace defrag

} // namespace afp


#endif // __NOEVENTSEQDEMUX_H_068FBC9538BBE3__

