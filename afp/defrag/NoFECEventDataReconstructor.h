/*******************************************************************************
*
* Copyright (c) 2009 Philipp Werner <philipp.werner@st.ovgu.de>
* All rights reserved.
*
*    Redistribution and use in source and binary forms, with or without
*    modification, are permitted provided that the following conditions
*    are met:
*
*    * Redistributions of source code must retain the above copyright
*      notice, this list of conditions and the following disclaimer.
*
*    * Redistributions in binary form must reproduce the above copyright
*      notice, this list of conditions and the following disclaimer in
*      the documentation and/or other materials provided with the
*      distribution.
*
*    * Neither the name of the copyright holders nor the names of
*      contributors may be used to endorse or promote products derived
*      from this software without specific prior written permission.
*
*
*    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
*    IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
*    TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
*    PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*    OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*    SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*    LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*
* $Id: $
*
******************************************************************************/


#ifndef __NOFECEVENTDATARECONSTRUCTOR_H_3E331BC5510F68__
#define __NOFECEVENTDATARECONSTRUCTOR_H_3E331BC5510F68__


#include <assert.h>


namespace afp {

namespace defrag {


/*!
 *	\brief	Policy: Reconstruct event data from fragments. Does not support
 *		forward error correction.
 *
 *	If you use this policy, NoFECHeaderSupport should be the FEC header
 *	policy of your choice.
 *
 *	Alternative policies: FECEventDataReconstructor
 */
template <class AFPRC>
class NoFECEventDataReconstructor {

	typedef typename AFPRC::elen_t elen_t;
	typedef typename AFPRC::flen_t flen_t;
	typedef typename AFPRC::fcount_t fcount_t;

	typedef class AFPRC::DefragStatistics Statistics;

private:
	
	uint8_t * col_data;	///< Restored data block
	elen_t col_length;	///< Length of the currently restored data (sum of put fragment's length)
	
	bool chunk_count_final;	///< false, if first fragment did not arrive (yet)
	fcount_t chunk_count;	///< Count of fragments the event consists of
	
	fcount_t col_chunks;	///< Count of data chunks collected.
	
	flen_t max_chunk_length;

public:

	/*!
	 *	\brief	Constructor
	 *	\param	max_chunk_length	Maximum payload of fragment (MTU - header)
	 */
	NoFECEventDataReconstructor(flen_t max_chunk_length)
		: col_data(0), col_length(0),
		  chunk_count_final(false), chunk_count(0),
		  col_chunks(0), max_chunk_length(max_chunk_length)
	{
	}
	
	/// Destructor
	~NoFECEventDataReconstructor()
	{
		fcount_t lost = chunk_count - col_chunks;
		Statistics::fragments_lost(chunk_count_final ? lost : lost + 1);

		if (get_data() == 0)
			Statistics::event_incomplete();

		if (col_data)
			delete [] col_data;
	}

	/*!
	 *	\brief	Process new fragment
	 *	\param	header	AFP header of the fragment
	 *	\param	data	Payload data of the fragment
	 *	\param	length	Payload data length of the fragment
	 */
	void put_fragment(const Headers<AFPRC> & header, const uint8_t * data, flen_t length)
	{
		assert(!header.fec.occurs());	// Cannot handle events using FEC
		assert(!get_data());		// Event should not be already complete
		assert(max_chunk_length != 0);
		
		if (header.first_fragment)
			chunk_count_final = true;
	
		if (!col_data) {
			// put_data_fragment() called first time
			chunk_count = header.fseq + (header.first_fragment ? 1 : 2);
			col_data = new uint8_t [chunk_count * max_chunk_length];
			Statistics::fragments_expected(chunk_count);
		}
		
		if (header.fseq >= chunk_count) {
			// Current chunk much too late. It has to be placed somewhere in front of
			// all data chunks received till now (fragment_count_final != true).
			Statistics::fragments_expected(header.fseq + 1 - chunk_count);
			realloc_col_data(header.fseq + 1);
		}
			
		// Chunk fits into col_data
		memcpy(col_data + (chunk_count - header.fseq - 1) * max_chunk_length, data, length);

		col_chunks++;
		col_length += length;

		Statistics::fragment_used();

		if (get_data() != 0)
			Statistics::event_complete();
	}

private:
	/*!
	 *	\brief Prepending realloc (add memory in front)
	 */
	void realloc_col_data(fcount_t new_chunk_count)
	{
		elen_t old_len = chunk_count * max_chunk_length;
		elen_t new_len = new_chunk_count * max_chunk_length;
		
		uint8_t * new_col_data = new uint8_t [new_len];
		memcpy(new_col_data + (new_len - old_len), col_data, old_len);
		delete [] col_data;
		
		col_data = new_col_data;
		chunk_count = new_chunk_count;
	}

public:
	/*!
	 *	\brief	Returns the defragmented data block if event is complete, NULL otherwise.
	 */
	uint8_t * get_data()
	{
		return (col_chunks == chunk_count && chunk_count_final) ? col_data : 0;
	}
	
	/*!
	 *	\brief	Returns the length of the event's data. Only valid if get_data() != 0.
	 */
	elen_t get_length()
	{
		return col_length;
	}
};


} // namespace defrag

} // namespace afp


#endif // __NOFECEVENTDATARECONSTRUCTOR_H_3E331BC5510F68__

