/*******************************************************************************
*
* Copyright (c) 2009 Philipp Werner <philipp.werner@st.ovgu.de>
* All rights reserved.
*
*    Redistribution and use in source and binary forms, with or without
*    modification, are permitted provided that the following conditions
*    are met:
*
*    * Redistributions of source code must retain the above copyright
*      notice, this list of conditions and the following disclaimer.
*
*    * Redistributions in binary form must reproduce the above copyright
*      notice, this list of conditions and the following disclaimer in
*      the documentation and/or other materials provided with the
*      distribution.
*
*    * Neither the name of the copyright holders nor the names of
*      contributors may be used to endorse or promote products derived
*      from this software without specific prior written permission.
*
*
*    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
*    IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
*    TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
*    PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*    OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*    SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*    LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*
* $Id: $
*
******************************************************************************/


#ifndef __EVENTSEQDEMUX_H_2A8B3BD61E7528__
#define __EVENTSEQDEMUX_H_2A8B3BD61E7528__


#include "Defragmenter.h"
#include "NoEventSeqDemux.h"

#include <map>
#include <list>
#include <assert.h>

#include "boost/thread/xtime.hpp"


// TODO: boost::pool einsetzen für new

namespace afp {

namespace defrag {


// TODO: Testing!!!
// TODO: Supports events without seq. numbers as well
/*!
 *	\brief	Policy: Event demultiplexer supporting event sequence numbers
 *
 *	If you use this policy, EventSeqHeaderSupport should be the event sequence number
 *	policy of your choice.
 *
 *	Alternatives: NoEventSeqDemux
 */
template <class AFPRC>
class  EventSeqDemux : public NoEventSeqDemux<AFPRC> {
	
	typedef typename AFPRC::elen_t   elen_t;
	typedef typename AFPRC::flen_t   flen_t;
	typedef typename AFPRC::fcount_t fcount_t;
	
private:
	
	/*!
	 *	\brief	Internal event representation
	 */
	class Event {
	
		typedef typename AFPRC::flen_t   flen_t;

	public:
		/// Defragmenter of the event
		Defragmenter<AFPRC> * def;

		/// Event sequence number of the event
		uint32_t eseq;

		// TODO: Absender (zusammenfassen mit eseq)

		/// Status of the event
		enum { event_incomplete, event_outdated } status;

		// TODO: turn event_incomplete to event_dropped after a long time
		/*!
		 *	\brief	Absolute time when event will be dropped.
		 *	
		 *	If status == event_outdated it is time after which this is removed from events map
		 *	and thus sequence number is free to be used again.
		 *	If status == event_incomplete is not yet implemented...
		 */
		boost::xtime drop_time;


		/// Constructor
		Event(flen_t max_payload, uint32_t eseq)
			: def(new Defragmenter<AFPRC>(max_payload)), eseq(eseq), status(def ? event_incomplete : event_outdated)
		{
		}

		/// Destructor
		~Event()
		{
			if (def) delete def;
		}

		/*!
		 *	\brief	Returns handle of this event
		 */
		void * to_handle()
		{
			return static_cast<void *>(this);
		}

		/*!
		 *	\brief	Returns event from handle
		 */
		static Event * from_handle(void * handle)
		{
			return static_cast<Event *>(handle);
		}
	};
	
	typedef std::map<uint32_t, Event *> EventMap;
	
	/// Assigns event events to event sequence numbers (and publisher address <- TODO)
	EventMap events;

	/// Completed/processed and dropped events (event seqence numbers and (publisher address <- TODO))
	std::list<Event *> outdated_events;

	/*!
	 *	\brief	Makes event outdated (event processed or dropped)
	 *	
	 *	Caller is responsible for deleting def.
	 */
	void set_event_outdated(Event * e)
	{
		e->def = 0;
		e->status = Event::event_outdated;

		// Keep event sequence number for some time to detect late duplicates.
		boost::xtime_get(&e->drop_time, boost::TIME_UTC);

		{
			// Use time for cleaning outdated events
			clean_outdated_events(e->drop_time);
		}

		e->drop_time.sec += 3;
		outdated_events.push_back(e);
	}

	/*!
	 *	\brief	Remove all outdated events with expired drop time
	 *
	 *	Function is called from set_event_outdated to save syscall for getting current time.
	 */
	void clean_outdated_events(const boost::xtime & curr_time)
	{
		Event * e;
		typename std::list<Event *>::iterator it = outdated_events.begin();
		while (it != outdated_events.end() && boost::xtime_cmp(curr_time, (*it)->drop_time) >= 0) {
			e = (*it);
//			printf("AFP: clean outdated event %u (seq can occur again)\n", (unsigned int)e->eseq);
			events.erase(e->eseq);
			delete e;
			it = outdated_events.erase(it);
		}
	}
	
public:

	/// Constructor
	EventSeqDemux(flen_t mtu)
		: NoEventSeqDemux<AFPRC>(mtu)
	{
	}
	
	/// Destructor
	~EventSeqDemux()
	{
		// TODO: mutex mechanism... destruction normally in other thread than subscribe_callback
		typename EventMap::iterator it = events.begin();
		for (; it != events.end(); it++)
			delete (*it).second;
	}

	/*!
	 *	\returns	Defragmenter handle, zero to drop the fragment.
	 */
	void * get_defragmenter_handle(const Headers<AFPRC> & header /* TODO: Absender-Adresse, Subject? */)
	{
		if (!header.eseq.occurs()) {
			// TODO: use base class functions (erkennung des handles über vergleich von member-zeiger)
			return 0;
		}

		uint32_t eseq = header.eseq.get_eseq();
		typename EventMap::iterator it = events.find(eseq);
		Event * event;

		if (it == events.end()) {
			// Unknown sequence number
			// -> create new event defragmenter
			assert(NoEventSeqDemux<AFPRC>::mtu > header.length());
			event = new Event(NoEventSeqDemux<AFPRC>::mtu - header.length(), eseq);
			events.insert(typename EventMap::value_type(eseq, event));
//			printf("AFP: getting fragment %u of NEW event %u\n", (unsigned int)header.fseq, (unsigned int)eseq);
			return event->to_handle();
		}

		// Found event
		event = (*it).second;
		assert(event->eseq == eseq);

		if (event->status == Event::event_outdated || event->def->get_event_data()) {
			// Event was already processed, dropped or is complete (late fragment of already dropped event, duplicate or FEC redundancy fragment not needed)
			// -> drop fragment
//			printf("AFP: dropping outdated fragment %u of event %u\n", (unsigned int)header.fseq, (unsigned int)eseq);
			return 0;
		}

//		printf("AFP: getting fragment %u of event %u\n", (unsigned int)header.fseq, (unsigned int)eseq);
		return event->to_handle();
	}
	
	/// Return Defragmenter from handle
	Defragmenter<AFPRC> * get_defragmenter(void * handle)
	{
		return Event::from_handle(handle)->def;
	}
	
	/*!
	 *	\brief	Frees defragmenter.
	 *	\param	handle	Defragmenter's handle
	 *
	 *	After calling keep_defragmenter(handle) use free_kept_defragmenter()
	 *	instead of this.
	 */
	void free_defragmenter(void * handle)
	{
		Event * e = Event::from_handle(handle);
		delete e->def;
		set_event_outdated(e);
	}


	/// Marks this policy to support late delivery of defragmented events (defragmenter
	/// not freed immediately after event is complete)
	enum { support_late_delivery };

	/*!
	 *	\brief	Keep defragmenter for later event delivery and return its new handle.
	 *	\param	handle	Defragmenter's handle
	 *	\return New handle you must pass to free_kep_defragmenter
	 *
	 *	After using this function use free_kept_defragmenter()
	 *	instead of free_defragmenter().
	 */
	void * keep_defragmenter(void * handle)
	{
		// Keep Defragmenter instance seperated from Event marked as processed.
		// It will be deleted in free_kept_defragmenter.
		Event * e = Event::from_handle(handle);
		Defragmenter<AFPRC> * def = e->def;
		set_event_outdated(e);
		return static_cast<void *>(def);
	}

	/*!
	 *	\brief	Get defragmenter from kept defragmenter's handle.
	 *	\param	handle	Handle returned by keep_defragmenter.
	 *	\return Defragmenter
	 */
	Defragmenter<AFPRC> * get_kept_defragmenter(void * handle)
	{
		return static_cast<Defragmenter<AFPRC> *>(handle);
	}
	
	/*!
	 *	\brief	Frees kept defragmenter.
	 *	\param	handle	Defragmenter's handle
	 *
	 *	After calling keep_defragmenter(handle) use this function
	 *	instead of free_defragmenter().
	 */
	void free_kept_defragmenter(void * handle)
	{
		delete get_kept_defragmenter(handle);
	}

};


} // namespace defrag

} // namespace afp


#endif // __EVENTSEQDEMUX_H_2A8B3BD61E7528__

