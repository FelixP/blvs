/*******************************************************************************
*
* Copyright (c) 2009 Philipp Werner <philipp.werner@st.ovgu.de>
* All rights reserved.
*
*    Redistribution and use in source and binary forms, with or without
*    modification, are permitted provided that the following conditions
*    are met:
*
*    * Redistributions of source code must retain the above copyright
*      notice, this list of conditions and the following disclaimer.
*
*    * Redistributions in binary form must reproduce the above copyright
*      notice, this list of conditions and the following disclaimer in
*      the documentation and/or other materials provided with the
*      distribution.
*
*    * Neither the name of the copyright holders nor the names of
*      contributors may be used to endorse or promote products derived
*      from this software without specific prior written permission.
*
*
*    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
*    IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
*    TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
*    PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*    OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*    SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*    LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*
* $Id: $
*
******************************************************************************/


#include "misc.h"

#include <assert.h>


namespace afp {


/*!
 *	\brief Splits a block of data into several pieces (fragments)
 *
 *	All fragments will be of equal size, last non-redundancy fragment may be smaller.
 *	Can be used for Abstract Network Layer and Application Layer (or wherever you want ^^)
 *
 *	\tparam AFPSC	AFPConfig
 */
template <class AFPSC>
class Fragmenter {

	typedef typename AFPSC::elen_t   elen_t;
	typedef typename AFPSC::flen_t   flen_t;
	typedef typename AFPSC::fcount_t fcount_t;
	
	typedef class AFPSC::FragEventSeqUsagePolicy EventSeqPolicy;
	typedef class AFPSC::FragFECUsagePolicy      FECPolicy;
	typedef class AFPSC::ArithmeticErrorChecking ArithmeticErrCheckPolicy;

protected:
	
	/// Length of the AFP basic header
	flen_t basic_header_length;
	
	/// Length of the payload
	flen_t payload_length;
	
	
	/// Data remaining to be fragmented 
	const uint8_t * remaining_data;
	
	/// Length of the data remaining to be fragmented (without FEC data fragments)
	elen_t remaining_length;
	
	
	/// Number of fragments still to be fetched via get_fragment()
	fcount_t remaining_fragments;
	
	/// Is next fragment is the first fragment?
	bool first_fragment;
	
	
	/// Event sequence number generator (may do nothing)
	EventSeqPolicy event_seq;
	
	/// Forward error correction (may do nothing)
	FECPolicy fec;
	
	/// Error indication
	ArithmeticErrCheckPolicy arith_err;

public:

	/*!	
	 *	\brief Construct a new fragmenter for an event
	 *	\param[in] event_data	Buffer containing the data to be fragmented
	 *	\param[in] event_length	Length of event_data buffer
	 *	\param[in] mtu		Maximum length of constructed fragments (header + payload)
	 */
	Fragmenter(const uint8_t * event_data, elen_t event_length, flen_t mtu)
		: remaining_data(event_data), remaining_length(event_length)
	{
		assert(mtu > 1 + EventSeqPolicy::header_length + FECPolicy::header_length);

		// Find header and payload size and the number of fragments

		flen_t header_length = 1 + EventSeqPolicy::header_length + FECPolicy::header_length;
		payload_length = mtu - header_length;

		elen_t k = div_round_up(event_length, (elen_t)payload_length);
		arith_err.check_equal(k, (fcount_t)k);
		
		remaining_fragments = FECPolicy::k2n(k);
		arith_err.check_smaller_or_equal(k, remaining_fragments);
		
		fcount_t max_fragments = 32;
		
		while (remaining_fragments > max_fragments &&
		       !arith_err.error())
		{
			header_length++;
			payload_length--;

			if (header_length >= mtu)
				break;
			
			k = div_round_up(event_length, (elen_t)payload_length);
			arith_err.check_equal(k, (fcount_t)k);

			remaining_fragments = FECPolicy::k2n(k);
			arith_err.check_smaller_or_equal(k, remaining_fragments);
			
			arith_err.check_smaller(max_fragments, max_fragments << 7);
			max_fragments <<= 7;
		}
		
//		printf("Fragmenter: %d bytes data -> %d bytes payload x %d fragments\n", (int)event_length, (int)payload_length, (int)remaining_fragments);
		
		if (header_length >= mtu || arith_err.error()) {
			arith_err.set_error();
			std::cerr << "AFP ERROR: Event of size " << event_length << " could not be fragmented due to an arithmetic error. Changing Dimensions config may help." << std::endl;
		}

		basic_header_length = header_length - EventSeqPolicy::header_length - FECPolicy::header_length;
		fec.init(event_length, payload_length, k);
		
		first_fragment = true;
	}
	
	/*!
	 *	\brief	Returns whether an error occured.
	 *
	 *	Check this after construction.
	 */
	bool error()
	{
		return arith_err.error();
	}

	/*!
	 *	\brief Write next fragment to fragment_data buffer.
	 *	\param[in]	fragment_data	Output buffer for fragment (at least mtu Bytes)
	 *	\returns	Length of data put into fragment buffer
	 */
	flen_t get_fragment(uint8_t * fragment_data)
	{
		if (!remaining_fragments)
			return 0;
		
		remaining_fragments--;
		
		// Insert header
		flen_t fragment_length = get_header(fragment_data);
		fragment_data += fragment_length;
		
		// Insert payload
		if (remaining_length == 0) {
		
			// Done with data fragments, get FEC redundancy fragment
			fec.get_red_fragment(fragment_data);
			fragment_length += payload_length;
			
		} else if (remaining_length >= payload_length) {
		
			// Get full data fragment
			memcpy(fragment_data, remaining_data, payload_length);
			fec.put_nonred_fragment(remaining_data);
			fragment_length += payload_length;
			
			remaining_length -= payload_length;
			remaining_data += payload_length;
			
		} else {
		
			// Get smaller data fragment (smaller than max. payload)
			memcpy(fragment_data, remaining_data, remaining_length);
			fec.put_smaller_nonred_fragment(remaining_data, remaining_length);
			fragment_length += remaining_length;
			
			remaining_length = 0;
		}
		
		return fragment_length;
	}
	
private:
	/*!
	 *	\brief	Write current fragment's header into buffer.
	 *	\param[in] 	Data buffer
	 *	\returns	Header length
	 */
	flen_t get_header(uint8_t * data)
	{
		// Write basic header
		{
			// Fragment sequence number
			fcount_t fseq = remaining_fragments;
			
			for (flen_t i = basic_header_length; i > 0 ; i--) {
				data[i-1] = fseq & 0xff;
				fseq >>= 8;
			}
			
			// TODO allgemeinere Implementierung
 			assert(basic_header_length <= 4);		// Following line won't produce right bits
			data[0] |= (0x38 << (4 - basic_header_length)) & 0x38;
			
			if (EventSeqPolicy::header_length || FECPolicy::header_length) {
				data[0] |= 0x80;
			}
			
			if (first_fragment) {
				data[0] |= 0x40;
				first_fragment = false;
			}
		
// 		printf("get_header():  seq=%u, header_len=%u, dump: %2x %2x %2x %2x\n", header_remaining_fragments, header_length, f_data[0], f_data[1], f_data[2], f_data[3]);
		}
		data += basic_header_length;
		
		// Write extension header: event sequence number
		event_seq.get_header(data, FECPolicy::header_length);
		data += EventSeqPolicy::header_length;
	
		// Write extension header: forward error correction
		fec.get_header(data, false);
		
		return basic_header_length + EventSeqPolicy::header_length + FECPolicy::header_length;
	}
};

} // namespace afp

