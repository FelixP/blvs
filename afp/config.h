/*******************************************************************************
*
* Copyright (c) 2009 Philipp Werner <philipp.werner@st.ovgu.de>
* All rights reserved.
*
*    Redistribution and use in source and binary forms, with or without
*    modification, are permitted provided that the following conditions
*    are met:
*
*    * Redistributions of source code must retain the above copyright
*      notice, this list of conditions and the following disclaimer.
*
*    * Redistributions in binary form must reproduce the above copyright
*      notice, this list of conditions and the following disclaimer in
*      the documentation and/or other materials provided with the
*      distribution.
*
*    * Neither the name of the copyright holders nor the names of
*      contributors may be used to endorse or promote products derived
*      from this software without specific prior written permission.
*
*
*    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
*    IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
*    TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
*    PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*    OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*    SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*    LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*
* $Id: $
*
******************************************************************************/


#ifndef __CONFIG_H_5586482BB69DFD__
#define __CONFIG_H_5586482BB69DFD__


// Policies
#include "frag/Use32BitEventSeq.h"
#include "frag/UseNoEventSeq.h"

#include "frag/UseNoFEC.h"
#include "frag/UseFEC.h"

#include "defrag/EventSeqHeaderSupport.h"
#include "defrag/NoEventSeqHeaderSupport.h"

#include "defrag/EventSeqDemux.h"
#include "defrag/NoEventSeqDemux.h"

#include "defrag/NoFECHeaderSupport.h"
#include "defrag/FECHeaderSupport.h"

#include "defrag/NoFECEventDataReconstructor.h"
#include "defrag/FECEventDataReconstructor.h"

#include "defrag/NoDuplicateChecker.h"
#include "defrag/DuplicateChecker.h"

#include "frag/RedundancyAttribute.h"

#include "defrag/Statistics.h"

#include "common/ErrorHandlingPolicies.h"


namespace afp {


// Application layer only
// Big event type
struct ApplicationLayerDimensions {
	typedef uint32_t fcount_t;	///< Fragment count type
	typedef uint16_t flen_t;	///< Length type for fragment
	typedef uint32_t elen_t;	///< Length type for fragmentation input (event)
};

struct AbstractNetworkLayerDimensions {
	typedef uint16_t fcount_t;	///< Fragment count type
	typedef uint16_t flen_t;	///< Length type for fragment
	typedef uint16_t elen_t;	///< Length type for fragmentation input (event)
};

// max. event 255 bytes
struct MinimalDimensions {
	typedef uint8_t fcount_t;	///< Fragment count type
	typedef uint8_t flen_t;		///< Length type for fragment
	typedef uint8_t elen_t;		///< Length type for fragmentation input (event)
};

// AVR...

template <class AFPC>
struct FragUseNoEventSeq : public frag::UseNoEventSeq<AFPC> {
};

template <class AFPC>
struct FragUseEventSeq : public frag::Use32BitEventSeq<AFPC> {
};

template <class AFPC>
struct FragUseNoFEC : public frag::UseNoFEC<AFPC> {
};

template <class AFPC>
struct FragUseFEC : public frag::UseFEC<AFPC> {
};


template <class AFPC>
struct DefragSupportNoEventSeq {
	typedef defrag::NoEventSeqHeaderSupport<AFPC> EventSeqHeaderPolicy;
	typedef defrag::NoEventSeqDemux<AFPC> DemuxPolicy;
};

template <class AFPC>
struct DefragSupportEventSeq {
	typedef defrag::EventSeqHeaderSupport<AFPC> EventSeqHeaderPolicy;
	typedef defrag::EventSeqDemux<AFPC> DemuxPolicy;
};

template <class AFPC>
struct DefragNoDuplicateChecking : public defrag::NoDuplicateChecker<AFPC> {
};

template <class AFPC>
struct DefragDuplicateChecking : public defrag::DuplicateChecker<AFPC> {
};

template <class AFPC>
struct DefragSupportNoFEC {
	typedef defrag::NoFECHeaderSupport<AFPC> FECHeaderPolicy;
	typedef defrag::NoFECEventDataReconstructor<AFPC> EventDataReconstructionPolicy;
};

template <class AFPC>
struct DefragSupportFEC {
	typedef defrag::FECHeaderSupport<AFPC> FECHeaderPolicy;
	typedef defrag::FECEventDataReconstructor<AFPC> EventDataReconstructionPolicy;
};

// TODO: nur Publisher-Config, nur Subscriber-Config
// TODO: Allokator, Logging
// TODO: compile time checkings: sizeof elen_t >= flen_t
// Dup -> Seq

class DefaultTag;

/*!
 *	\brief	Template for creating AFP configurations
 *
 *	TODO
 */
template <
	class Dimensions,
        template <class Config> class FragEventSeqPolicy,
	template <class Config> class FragFECPolicy,
	template <class Config> class DefragEventSeqPolicy,
	template <class Config> class DefragDuplicatePolicy,
	template <class Config> class DefragFECPolicy,
	class FragRedundancyAttribute = frag::RedundancyAttribute<DefaultTag, 0>,
	class DefragStatisticsPolicy = defrag::Statistics<DefaultTag>,
	class ArithmeticErrorCheckingPolicy = common::ArithmeticErrorChecking
>
struct AFPConfig {

	typedef AFPConfig ThisConfigType;

	typedef typename Dimensions::fcount_t fcount_t;		///< Fragment count type
	typedef typename Dimensions::flen_t flen_t;		///< Length type for fragment
	typedef typename Dimensions::elen_t elen_t;		///< Length type for fragmentation input (event)

	typedef FragEventSeqPolicy<ThisConfigType> FragEventSeqUsagePolicy;
	typedef FragFECPolicy<ThisConfigType> FragFECUsagePolicy;

	
	typedef typename DefragEventSeqPolicy<ThisConfigType>::EventSeqHeaderPolicy DefragEventSeqHeaderPolicy;
	typedef typename DefragEventSeqPolicy<ThisConfigType>::DemuxPolicy DefragDemuxPolicy;


	typedef DefragDuplicatePolicy<ThisConfigType> DefragDuplicateCheckingPolicy;


	typedef typename DefragFECPolicy<ThisConfigType>::FECHeaderPolicy DefragFECHeaderPolicy;
	typedef typename DefragFECPolicy<ThisConfigType>::EventDataReconstructionPolicy DefragEventDataReconstructionPolicy;


	typedef FragRedundancyAttribute FragRedundancy;

	typedef DefragStatisticsPolicy DefragStatistics;

	typedef ArithmeticErrorCheckingPolicy ArithmeticErrorChecking; 
};


typedef afp::AFPConfig <
	afp::ApplicationLayerDimensions,
	afp::FragUseEventSeq,
	afp::FragUseNoFEC,
	afp::DefragSupportEventSeq,
	afp::DefragNoDuplicateChecking,
	afp::DefragSupportNoFEC
> ApplicationLayerDefaultConfig;


// TODO: Einige Configs vorgeben


} // namespace afp


#endif // __CONFIG_H_5586482BB69DFD__

