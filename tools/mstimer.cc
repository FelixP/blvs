#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>

int DrawText(SDL_Surface* screen, TTF_Font* font, const char* text)
{
    SDL_Color color = {255,255,255};
    SDL_Surface *text_surface;

    text_surface = TTF_RenderText_Solid(font, text, color);
    if (text_surface != NULL)
    {
        SDL_BlitSurface(text_surface, 0, screen,0);
        SDL_FreeSurface(text_surface);
        return 1;
    }
    else
    {
       return 0;
    }
}

int main (int argc, char **argv)
{
	if (SDL_Init(SDL_INIT_VIDEO|SDL_INIT_TIMER) < 0) {
		fprintf(stderr, "Couldn't init SDL : %s \n", SDL_GetError());
		exit(1);
	}
	uint32_t surface_flags= SDL_HWSURFACE|SDL_ANYFORMAT|SDL_RESIZABLE|SDL_HWACCEL;
	
	SDL_Surface * screen = SDL_SetVideoMode(1680, 400, 24, surface_flags);
	if (screen == NULL) {
		fprintf(stderr, "Couldn't set video mode : %s\n",SDL_GetError());
		exit(1);
	}
	if(TTF_Init()==-1) {
		printf("TTF_Init: %s\n", TTF_GetError());
		exit(2);
	}
	TTF_Font * font=TTF_OpenFont("Vera.ttf", 260);
	if(!font) {
		printf("TTF_OpenFont: %s\n", TTF_GetError());
		exit(2);
	}
	uint32_t bg_color = SDL_MapRGB(screen->format,0,0,0);

	SDL_Event sdl_event;
	bool term=false;
	while(!term)
	{
		while(SDL_PollEvent(&sdl_event))
			switch(sdl_event.type)
			{
				case SDL_VIDEORESIZE:
					screen = SDL_SetVideoMode(sdl_event.resize.w,sdl_event.resize.h, 24, surface_flags);
					if ( screen == 0 )
					{
						fprintf(stderr, "Resize fehlgeschlagen: %s",SDL_GetError());
						return 0;
					}
					printf("%i x %i \n",screen->w,screen->h);
					break;
				case SDL_QUIT:
					term=true;
					break;
				default:
					break;
			}
		char time[100];
		sprintf(time,"%i",SDL_GetTicks());
		SDL_FillRect(screen, 0, bg_color);
		DrawText(screen, font, time);
		SDL_Flip(screen);
	}

	TTF_CloseFont(font);
	TTF_Quit();
	SDL_Quit();
}