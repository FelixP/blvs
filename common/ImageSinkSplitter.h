#ifndef __IMAGESINKSPLITTER_H_F692B6E015875C__
#define __IMAGESINKSPLITTER_H_F692B6E015875C__

template <class Sink1, class Sink2>
class ImageSinkSplitter {
	
	Sink1 & _s1;
	Sink2 & _s2;

public:
	struct YUYV {uint8_t y0 ,u0 ,y1 ,v0;enum{pixel_size=2};}__attribute__((packed));
	struct YU12 {uint8_t* y ,*u ,*v; enum{uscaleh=1,uscalev=1,vscaleh=1,vscalev=1};};

	ImageSinkSplitter(Sink1 & s1, Sink2 & s2)
		: _s1(s1), _s2(s2)
	{
	}

	ImageSinkSplitter & put_image(YUYV * src, uint16_t width, uint16_t height)
	{
		_s1.put_image((typename Sink1::YUYV *)src, width, height);
		_s2.put_image((typename Sink2::YUYV *)src, width, height);
		return *this;
	}

	ImageSinkSplitter & put_image(YU12 * src, uint16_t width, uint16_t height)
	{
		fprintf(stderr, "Error: No Support for direct recoding since compiled to use YU12");
		exit(1);
		return *this;
	}
};


#endif // __IMAGESINKSPLITTER_H_F692B6E015875C__

