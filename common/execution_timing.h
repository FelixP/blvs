#ifndef EXECUTION_TIMING_H
#define EXECUTION_TIMING_H
#ifdef DO_TIMING

#include <stdio.h>
#include <time.h>
#include <sys/time.h>

/**
 *	Runs \p cmd if DO_TIMING is declared. Otherwise cmd disappears.
 */
#define timing_command(cmd) cmd

/**
 *	Declares a symbol \p var as class member for holding return value of __clock_start().
 */
#define clock_member_decl(var) unsigned int (var)

/**
 *	Initializes a symbol \p var as class member for holding return value of __clock_start().
 */
#define clock_member_init(var) (var) = 0

/**
 *	Declares a symbol \p var for holding return value of __clock_start().
 */
#define clock_decl(var) unsigned int (var) = 0

static inline unsigned int __clock_start() 
{
	struct timeval t;
	gettimeofday(&t, 0);
	return t.tv_sec * 1000 + t.tv_usec / 1000;	// time in ms
}

/**
 *	Start the clock for time measurement. Does not declare a symbol.
 *	\returns	
 */
#define clock_start(var) (var) = __clock_start()

/**
 *	Start the clock for time measurement. Declares a symbol.
 *	\note		Combines clock_decl and __clock_start, but can't be
 *			used everywhere in C code.
 *	\param	var	An identifier for a variable that will hold the starting time.
 */
#define clock_start_decl(var) unsigned int (var) = __clock_start()


static inline unsigned int __clock_stop(unsigned int start)
{
	struct timeval t;
	gettimeofday(&t, 0);
	unsigned int stop = t.tv_sec * 1000 + t.tv_usec / 1000;
	return (stop - start);		// time in ms
}

/**
 *	Stop the clock and return time difference to \p start.
 *	\param	start	Starting time. \see clock_start
 *	\returns	Time difference in ms.
 */
#define clock_stop(start) __clock_stop((start))


static inline void __clock_stop_sum(unsigned int start, unsigned int * sum)
{
	unsigned int ms = clock_stop(start);
	*sum += ms;
}

/**
 *	Stop the clock (time difference to \p start) and add difference to \p sum.
 *	\param	start	Starting time. \see clock_start
 *	\param	sum	Time variable to sum times (in ms).
 *	\returns	Time difference in ms.
 */
#define clock_stop_sum(start, sum) __clock_stop_sum((start), &(sum))


static inline void __clock_stop_msg(unsigned int start, const char * str)
{
	unsigned int ms = clock_stop(start);
	fprintf(stderr, "<timing> %s: %u ms\n", str, ms);
}

/**
 *	Stop the clock and print timing info to stderr.
 *	\param	start	Starting time. \see clock_start
 *	\param	str	String printed to identify the timing
 */
#define clock_stop_msg(start,str) __clock_stop_msg((start),(str))


static inline void __clock_stop_bps_msg(unsigned int start, const char * str, unsigned long bytes)
{
	unsigned int ms = clock_stop(start);
	unsigned long kbytes = bytes >> 10;
	if (ms)
		fprintf(stderr, "<timing> %s: %u ms, %lu KB, %lu KB/s\n",
				str, ms, (unsigned long)kbytes, (unsigned long)kbytes * 1000 / ms);
	else
		fprintf(stderr, "<timing> %s: %u ms, %lu KB\n", str, ms, (unsigned long)kbytes);
}

/**
 *	Stop the clock and print timing and data rate info to stderr.
 *	\param	start	Starting time. \see clock_start
 *	\param	str	String printed to identify the timing
 *	\param	bytes	Number of bytes. Needed for calculation of data rate.
 */
#define clock_stop_bps_msg(start,str,bytes) __clock_stop_bps_msg((start),(str),(bytes))

static inline void __clock_mean_msg(unsigned int sum, unsigned int count, const char * str)
{
	if (!count)
		return;
	float mean = ((float) sum) / ((float) count);
	fprintf(stderr, "<timing> %s: %.2f ms\n", str, mean);
}

/**
 *	Print aritmetic mean time (\p sum / \p count) to stderr.
 *	\param	sum	Time in ms	
 *	\param	count	Count of times added to \p sum
 *	\param	str	String printed to identify the timing
 */
#define clock_mean_msg(sum,count,str) __clock_mean_msg((sum),(count),(str))


#else

#define timing_command(cmd)
#define clock_member_decl(var)
#define clock_member_init(var)
#define clock_decl(var)
#define clock_start(var)
#define clock_start_decl(var)
#define clock_stop(start)
#define clock_stop_sum(start, sum)
#define clock_stop_msg(start,str)
#define clock_stop_bps_msg(start,str,bytes)
#define clock_mean_msg(sum,count,str)

#endif
#endif
