// verbosity
#include <stdio.h>

#ifndef VERBOSITY_H
#define VERBOSITY_H

#ifndef MAX_VERBOSITY
#define MAX_VERBOSITY 5
#endif //MAX_VERBOSITY

class Verbosity {
	static int & level()
	{
		static int vl = 0;
		return vl;
	}
public:
	static int get_level()
	{
		return level();
	}
	static int set_level(int i)
	{
		if (i > MAX_VERBOSITY || i < 0)
		{
			fprintf(stderr,"Verbosity Level not suported\n");
			return 1;
		}
		else
		{
			level() = i;
			return 0;
		}
	}
};

// Use SUPPORT_VERBOSITY_X to insert code that are nessary to support verbosity-level X (e.g.: data collection variables)
// Use VERBOSITY_X to calculate and output the collected data this code is only run if the corresponding level is set
// Attention: the commands are run in their own codeblock
// if you want to run more than one command do VERBOSITY_X({ //put your commands here (even more than oneline is possible) });

// regelmäßige Ausgaben (zb pro Frame) zuvor bekannt als 4
#if MAX_VERBOSITY >= 5
#define SUPPORT_VERBOSITY_5(cmd) cmd
#else
#define SUPPORT_VERBOSITY_5(cmd)
#endif

#define VERBOSITY_5(cmd) SUPPORT_VERBOSITY_5(do{ if( Verbosity::get_level() >= 5) {cmd;} }while(0))

// regelmäßige Ausgaben max genau 1 Zeilen pro Frame und Modul
#if MAX_VERBOSITY >= 4
#define SUPPORT_VERBOSITY_4(cmd) cmd
#else
#define SUPPORT_VERBOSITY_4(cmd)
#endif

#define VERBOSITY_4(cmd) SUPPORT_VERBOSITY_4(do{ if( Verbosity::get_level() >= 4) {cmd;} }while(0))

// unregelmäßige aber möglicherweise häufige ereignisse (frame verwerfen)
#if MAX_VERBOSITY >= 3
#define SUPPORT_VERBOSITY_3(cmd) cmd
#else
#define SUPPORT_VERBOSITY_3(cmd)
#endif

#define VERBOSITY_3(cmd) SUPPORT_VERBOSITY_3(do{ if( Verbosity::get_level() >= 3) {cmd;} }while(0))

// Hardwareinfo und Gründe für (Auto-)Adaption
#if MAX_VERBOSITY >= 2
#define SUPPORT_VERBOSITY_2(cmd) cmd
#else
#define SUPPORT_VERBOSITY_2(cmd)
#endif

#define VERBOSITY_2(cmd) SUPPORT_VERBOSITY_2(do{ if( Verbosity::get_level() >= 2) {cmd;} }while(0))

// (Auto)Adaptions-Entscheidungen und einmalige Performace-Ausgaben (beim Start oder Beenden (Scrollgefahr ->0))
#if MAX_VERBOSITY >= 1
#define SUPPORT_VERBOSITY_1(cmd) cmd
#else
#define SUPPORT_VERBOSITY_1(cmd)
#endif

#define VERBOSITY_1(cmd) SUPPORT_VERBOSITY_1(do{ if( Verbosity::get_level() >= 1) {cmd;} }while(0))


// Verbosity_level setzen
#define SET_VERBOSITY(i) Verbosity::set_level( (i) )

#endif //VERBOSITY_H