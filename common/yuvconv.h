
namespace YUVconv
{
	struct YU12 {
		uint8_t* y ,*u ,*v;
		uint height, width;
		enum{	yscaleh=0,yscalev=0,ydist=1,
		uscaleh=1,uscalev=1,udist=1,
		vscaleh=1,vscalev=1,vdist=1
		};
	};
	struct YUYV {
		uint8_t* y ,*u ,*v;
		uint height, width;

		YUYV(uint8_t &data)
		{
			y=&data;
			u=&data +1;
			v=&data +3;
		}

		enum{	yscaleh=0,yscalev=0,ydist=2,
		uscaleh=1,uscalev=0,udist=4,
		vscaleh=1,vscalev=0,vdist=4
		};
	};

	template<typename infmt,typename outfmt> void convert(infmt &in, outfmt &out)
	{
		if(!((in.height == out.height) && (in.width == out.width)))
			return; //fehler
		//TODO	((infmt.eyeryscale >= 0) && (outfmt.eyeryscale >= 0)) || (return,false); //fehler
		for (uint line= 0; line<out.height; line += (1<<out.yscalev) )
		{
			unsigned int outlinepos	= (line >> out.yscalev) * (out.width);
			unsigned int inlinepos	= (line >> in.yscalev) * (in.width);
			for(uint pos=0; pos < out.width; pos +=(1<<out.yscaleh))
				out.y[((outlinepos+pos)>>out.yscaleh)*out.ydist] = in.y[((inlinepos+pos)>>in.yscaleh)*in.ydist];
		}
		for (uint line= 0; line<out.height; line += (1<<out.uscalev) )
		{
			unsigned int outlinepos	= (line >> out.uscalev) * (out.width);
			unsigned int inlinepos	= (line >> in.uscalev) * (in.width);
			for(uint pos=0; pos < out.width; pos +=(1<<out.uscaleh))
				out.u[((outlinepos+pos)>>out.uscaleh)*out.udist] = in.u[((inlinepos+pos)>>in.uscaleh)*in.udist];
		}
		for (uint line= 0; line<out.height; line += (1<<out.vscalev) )
		{
			unsigned int outlinepos	= (line >> out.vscalev) * (out.width);
			unsigned int inlinepos	= (line >> in.vscalev) * (in.width);
			for(uint pos=0; pos < out.width; pos +=(1<<out.vscaleh))
				out.v[((outlinepos+pos)>>out.vscaleh)*out.vdist] = in.v[((inlinepos+pos)>>in.vscaleh)*in.vdist];
		}
	}
}
