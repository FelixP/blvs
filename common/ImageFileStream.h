#ifndef __IMAGEFILESTREAM_H_39527D0E863290__
#define __IMAGEFILESTREAM_H_39527D0E863290__

#include <assert.h>

#include <boost/thread/mutex.hpp>
#include <boost/thread/xtime.hpp>

#include "../common/execution_timing.h"

// Getting directory content
#include <sys/types.h>
#include <dirent.h>
#include <vector>
#include <string>
#include <algorithm>

// mmap
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>



 
/*!
 *	\brief	Image source: reads raw YUYV files from directory
 */
class ImageFileStream
{

	/*!
	 *	\brief	Adds directory delimiter if necessary and fills files with directory entries (*.yuyv)
	 *	\param	dir	[in/out]	Directory path
	 *	\param	files	[out]		YUYV files in dir
	 *	\return Non-zero on error
	 */
	int getdirsorted(std::string & dir, std::vector<std::string> & files)
	{
		DIR *dp;
		struct dirent *dirp;
		if((dp  = opendir(dir.c_str())) == NULL) {
			perror((std::string("Error openening directory ") + dir).c_str());
			return -1;
		}
		while ((dirp = readdir(dp)) != NULL) {
			if (strcmp(dirp->d_name + strlen(dirp->d_name) - 5, ".yuyv") == 0) {
				files.push_back(std::string(dirp->d_name));
// 				printf("%s: %s\n", dir.c_str(), dirp->d_name);
			}
		}
		closedir(dp);
		sort(files.begin(), files.end());
		if (dir[dir.size() - 1] != '/')
			dir += '/';
		return 0;
	}

	/*!
	 *	\brief	Map file into memory
	 *	\return Non-zero on error
	 */
	int mmap_file(const char * file, void *& data, size_t & size)
	{
		int fd;
		struct stat sbuf;
	
		if ((fd = open(file, O_RDONLY)) == -1) {
			perror("open");
			exit(1);
		}
	
		if (stat(file, &sbuf) == -1) {
			perror("stat");
			exit(1);
		}
	
		if ((data = mmap((caddr_t)0, sbuf.st_size, PROT_READ, MAP_SHARED, fd, 0)) == (caddr_t)(-1)) {
			perror("mmap");
			exit(1);
		}

		size = sbuf.st_size;
	
		return fd;
	}

	/*!
	 *	\brief	Unmap file from memory
	 */
	void munmap_file(int fd, void * data, size_t size)
	{
		munmap(data, size);
		close(fd);
	}

	unsigned int width, height;
	unsigned int frame_wait_time_ms;

	std::string dir;
	std::vector<std::string> filenames;

	std::vector<std::string>::iterator filename_it;
	

	/*!
	 *	\brief	Return filename of next image to load
	 */
	const std::string & get_next_filename()
	{
		const std::string & ret = *filename_it;

		filename_it++;

		if (filename_it == filenames.end())
			filename_it = filenames.begin();

		return ret;
	}

public:

	/*!
	 *	\brief	Constructor
	 *	\param	directory	Directory containing YUYV files
	 *	\param	width		Width of the YUYV images in directory
	 *	\param	height		Height of the YUYV images in directory
	 */
	ImageFileStream(const char * directory, unsigned int width, unsigned int height)
		: width(width), height(height), dir(directory)
	{
		// Aim: 10 fps... TODO
		frame_wait_time_ms = 45;

		if (getdirsorted(dir, filenames) == -1)
			exit(-1);

		filename_it = filenames.begin();

		if (filenames.empty())
		{
			fprintf(stderr, "Did not find any YUYV file (*.yuyv) in %s\n", directory);
			exit(-1);
		}
	}


	/*!
	 *	\brief	Give next image from this image source to imgstore
	 */
	template<class T> T& give_image(T& imgstore)
	{
		void * data;
		size_t length;
		std::string path = dir + get_next_filename();

		boost::xtime next_time;
		boost::xtime_get(&next_time, boost::TIME_UTC);
		
		next_time.nsec += frame_wait_time_ms * 1000000;

		// Load file
		int fd = mmap_file(path.c_str(), data, length);

		if (length != width*height*2) {
			fprintf(stderr, "YUYV file %s is not %dx%d!\n", path.c_str(), width, height);
			exit(-1);
		}
		
		// Wait until it is time to process the image
		boost::thread::sleep(next_time);

		// Process image
		imgstore.put_image((typename T::YUYV *)data, width, height);

		munmap_file(fd, data, length);

		return imgstore;
	}
	
};

#endif // __IMAGEFILESTREAM_H_39527D0E863290__

