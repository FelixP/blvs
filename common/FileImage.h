/*!
 *	\brief	Image sink: write image into image file (standard: YUYV)
 */
class FileImage
{
	public:
		struct RGB24 {uint8_t r,g,b; enum{pixel_size=3};}__attribute__((packed));
		struct YUYV {uint8_t y0 ,u0 ,y1 ,v0;enum{pixel_size=2};}__attribute__((packed));
		template< int D_pixel_size> struct D{enum{pixel_size=D_pixel_size};};
	private:
		int fd;
	public:

		FileImage(const char * filename)
		{
			fd = open(filename, O_RDWR|O_CREAT|O_TRUNC, 0644);
			if (fd < 0)
			{
				perror ("openfile");
				exit (EXIT_FAILURE);
			}
		}

		~FileImage()
		{
			close(fd);
		}

		template<class T> FileImage& put_image(T * src,uint16_t width,uint16_t height)
		{
			write(fd,src, T::pixel_size * width * height);
			return *this;
		}

		FileImage& put_image(uint8_t * src,uint16_t width,uint16_t height)
		{
			write(fd,src, width * height);
			return *this;
		}
};
