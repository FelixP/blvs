#ifndef CIMGIM_H
#define CIMGIM_H

#include <CImg/CImg.h>

#ifndef DataT
#define DataT int32_t
#endif

class CImgIM: public cimg_library::CImg< uint8_t >
{
	public:
		struct RGB24 {uint8_t r,g,b;}__attribute__((packed));
		struct YUYV {uint8_t y0 ,u0 ,y1 ,v0;}__attribute__((packed));
		struct PGFYUV{DataT * y, * u , * v; bool undersampled;};
		struct YU12 {uint8_t* y ,*u ,*v; enum{uscaleh=1,uscalev=1,vscaleh=1,vscalev=1};};

	private:
		template<class T> uint8_t _crop(T h)
		{
			return h>255?255:h<0?0:h;
		}
		
		CImgIM& from_yuv(const YUYV * yuyv,const unsigned int dimw,const unsigned int dimh)
		{
			#ifdef YUYV2RGB
			assign(dimw,dimh,1,3);
			uint8_t *pR = ptr(0,0,0,0), *pG = ptr(0,0,0,1), *pB=ptr(0,0,0,2);
			// printf("format: %lx %i %i\n",(unsigned long) rgb,dimw,dimh);
			const YUYV * ptrs = yuyv;
			for (unsigned int off= this->width * this->height/2; off>0; --off) 
			{
				#if 0
				*(pR++) = (uint8_t) _crop(ptrs->y0 + (1.370705 * (ptrs->v0 - 128)));
				*(pG++) = (uint8_t) _crop(ptrs->y0 - (0.698001 * (ptrs->v0 - 128)) - (0.337633 * (ptrs->u0-128)));
				*(pB++) = (uint8_t) _crop(ptrs->y0 + (1.732446 * (ptrs->u0-128)));
				*(pR++) = (uint8_t) _crop(ptrs->y1 + (1.370705 * (ptrs->v0 - 128)));
				*(pG++) = (uint8_t) _crop(ptrs->y1 - (0.698001 * (ptrs->v0 - 128)) - (0.337633 * (ptrs->u0-128)));
				*(pB++) = (uint8_t) _crop(ptrs->y1 + (1.732446 * (ptrs->u0-128)));
				#else
				int c = ptrs->y0 -16, d = ptrs->u0 - 128, e = ptrs->v0 - 128;       
				*(pR++) = (uint8_t) _crop((298 * c           + 409 * e + 128) >> 8);
				*(pG++) = (uint8_t) _crop((298 * c - 100 * d - 208 * e + 128) >> 8);
				*(pB++) = (uint8_t) _crop((298 * c + 516 * d           + 128) >> 8);
				c = ptrs->y1 -16;
				*(pR++) = (uint8_t) _crop((298 * c           + 409 * e + 128) >> 8);
				*(pG++) = (uint8_t) _crop((298 * c - 100 * d - 208 * e + 128) >> 8);
				*(pB++) = (uint8_t) _crop((298 * c + 516 * d           + 128) >> 8);
				#endif
				ptrs ++;
				
			}
			return *this;
			#else
			#ifdef SW
			assign(dimw,dimh);
			uint8_t *pY = ptr(0,0);
			const uint8_t *ptrs =(const uint8_t *) yuyv;
			for (unsigned int off= this->width * this->height; off>0; --off) {
				*(pY++) = (uint8_t)*(ptrs);
				ptrs+=2;
			}
			return *this;
			#else
			#ifdef SUP
			assign(dimw,dimh,1,2);
			uint8_t *pY = ptr(0,0,0,1), *pU = ptr(0,0,0,0);
			const uint8_t *ptrs =(const uint8_t *) yuyv;
			
			for (unsigned int off= this->width * this->height; off>0; --off) 
			{
				*(pY++) = (uint8_t)*(ptrs++);
				*(pU++) = (uint8_t)*(ptrs++);
			}
			return *this;
			#else
			assign(dimw,dimh,1,3);
			uint8_t *pY = ptr(0,0,0,1), *pU = ptr(0,0,0,2),*pV = ptr(0,0,0,0); 
			const uint8_t *ptrs =(const uint8_t *) yuyv;
			bool isU= false;
			for (unsigned int off= this->width * this->height; off>0; --off) 
			{
				if( (isU = !isU) )
				{
					*(pY++) = (uint8_t)*(ptrs++);
					*(pU++) = (uint8_t)*(ptrs);
					*(pU++) = (uint8_t)*(ptrs++);
				}else
				{
					*(pY++) = (uint8_t)*(ptrs++);
					*(pV++) = (uint8_t)*(ptrs);
					*(pV++) = (uint8_t)*(ptrs++);
				}
			}
			return *this;
			
			#endif
			#endif
			#endif
			//
		}
		
		CImgIM& from_yu12(const YU12 * yu12,const unsigned int dimw,const unsigned int dimh)
		{
			assign(dimw,dimh,1,3);
			uint8_t *pR = ptr(0,0,0,0), *pG = ptr(0,0,0,1), *pB=ptr(0,0,0,2);
			uint8_t *iny = yu12->y;
			uint8_t *inu = yu12->u;
			uint8_t *inv = yu12->v;
		
			for (unsigned int line= 0; line<dimh; line++)
			{
				unsigned int yline= line * dimw;
				unsigned int uline= (line>>YU12::uscalev) * (dimw>>(YU12::uscaleh));
				unsigned int vline= (line>>YU12::vscalev) * (dimw>>(YU12::vscaleh));
				for(unsigned int pos=0; pos < dimw ;pos++)
				{
					int yp = iny[yline+ (pos)];
					int up = inu[uline+ (pos>>(YU12::uscaleh))];
					int vp = inv[vline+ (pos>>(YU12::vscaleh))];
					int c = yp -16, d = up - 128, e = vp - 128;
					*(pR++) = (uint8_t) _crop((298 * c           + 409 * e + 128) >> 8);
					*(pG++) = (uint8_t) _crop((298 * c - 100 * d - 208 * e + 128) >> 8);
					*(pB++) = (uint8_t) _crop((298 * c + 516 * d           + 128) >> 8);
				
				}
			}
			return *this;
		}
		
		CImgIM& from_rgb(uint8_t const* rgb,const unsigned int dimw,const unsigned int dimh)
		{
			assign(dimw,dimh,1,3);
			uint8_t *pR = ptr(0,0,0,0), *pG = ptr(0,0,0,1), *pB=ptr(0,0,0,2);
			// printf("format: %lx %i %i\n",(unsigned long) rgb,dimw,dimh);
			const uint8_t *ptrs = rgb;
			for (unsigned int off= this->width * this->height; off>0; --off) {
				*(pR++) = (uint8_t)*(ptrs++);
				*(pG++) = (uint8_t)*(ptrs++);
				*(pB++) = (uint8_t)*(ptrs++);
				
			}
			return *this;
		}
		
		CImgIM& from_uk(uint8_t const* rgb,const unsigned int dimw,const unsigned int dimh)
		{
			assign(dimw,dimh,1,1);
			uint8_t *pR = ptr(0,0,0,0);
			// printf("format: %lx %i %i\n",(unsigned long) rgb,dimw,dimh);
			const uint8_t *ptrs = rgb;
			for (unsigned int off= this->width * this->height; off>0; --off) {
				*(pR++) = (uint8_t)*(ptrs++);
			}
			return *this;
		}
		
		
	public:
		
		template<class T> CImgIM& put_image(T * src,uint16_t width,uint16_t height)
		{
			return from_uk((uint8_t * ) src,width,height);
		}
		
		CImgIM& put_image(PGFYUV * src,uint16_t width,uint16_t height)
		{
			assign(width,height,1,3);
			DataT * y = src->y;
			DataT * u = src->u;
			DataT * v = src->v;
			bool m_downsample = src->undersampled;
			
			uint8_t *pR = ptr(0,0,0,0), *pG = ptr(0,0,0,1),*pB = ptr(0,0,0,2);
			{
				int sampledPos = 0, yPos = 0;
				const bool wOdd = (1 == width%2);
				DataT g , uAvg, vAvg;
				for (uint16_t i=0; i < height; i++)
				{
					if (i%2) sampledPos -= (width + 1)/2;
					for (uint16_t j=0; j < width; j++) 
					{
						if (m_downsample) {
							// image was downsampled
							uAvg = u[sampledPos];
							vAvg = v[sampledPos];
						} else {
							uAvg = u[yPos];
							vAvg = v[yPos];
						}
						// Yuv
						*pG++ = g = _crop(y[yPos] + 128 - ((uAvg + vAvg ) >> 2)); // must be logical shift operator
						*pR++ = _crop(uAvg + g);
						*pB++ = _crop(vAvg + g);
						yPos++; 
						if (j%2) sampledPos++;
					}
					if (wOdd) sampledPos++;
				}
			}
			return *this;
			
		}
		
		CImgIM& put_image(RGB24 * src,uint16_t width,uint16_t height)
		{
			return from_rgb((uint8_t * ) src,width,height);
		}

		CImgIM& put_image(YU12 * src,uint16_t width,uint16_t height)
		{
			return from_yu12(src,width,height);
		}
		
		CImgIM& put_image(YUYV * src,uint16_t width,uint16_t height)
		{
			return from_yuv(src,width,height);
		}
		
		template<class T> T& give_image(T& imgstore)
		{
			uint8_t *pR = ptr(0,0,0,0), *pG = ptr(0,0,0,1), *pB=ptr(0,0,0,2);
			int R,G,B;
			uint8_t Y,U,V;
			uint8_t * pic = (typeof pic)malloc(dimx()*dimy()*2);
			int off = 0;
			
			for (int i = 0; i < dimy() * dimx(); i+=2)
			{
				#if 0
				//get RGB
				R=*pR++; G=*pG++;B=*pB++;
				
				//calc YUV
				Y =  (0.257 * R) + (0.504 * G) + (0.098 * B) + 16;
				V =  (0.439 * R) - (0.368 * G) - (0.071 * B) + 128;
				U = -(0.148 * R) - (0.291 * G) + (0.439 * B) + 128;
				
				//put YU
				pic[off++] = Y;
				pic[off++] = U;
				
				//get RGB
				R=*pR++; G=*pG++;B=*pB++;
				
				//calc YUV
				Y =  (0.257 * R) + (0.504 * G) + (0.098 * B) + 16;
				V =  (0.439 * R) - (0.368 * G) - (0.071 * B) + 128;
				U = -(0.148 * R) - (0.291 * G) + (0.439 * B) + 128;
				
				//put YV
				pic[off++] = Y;
				pic[off++] = V;
				
				#else
				
				//get RGB
				R=*pR++; G=*pG++;B=*pB++;
				//calc YUV
				Y = ( (  66 * R + 129 * G +  25 * B + 128) >> 8) +  16;
				U = ( ( -38 * R -  74 * G + 112 * B + 128) >> 8) + 128;
				V = ( ( 112 * R -  94 * G -  18 * B + 128) >> 8) + 128;
				//put YU
				pic[off++] = Y;
				pic[off++] = U;
				
				//get RGB
				R=*pR++; G=*pG++;B=*pB++;
				//calc YUV
				Y = ( (  66 * R + 129 * G +  25 * B + 128) >> 8) +  16;
				U = ( ( -38 * R -  74 * G + 112 * B + 128) >> 8) + 128;
				V = ( ( 112 * R -  94 * G -  18 * B + 128) >> 8) + 128;
				//put YV
				pic[off++] = Y;
				pic[off++] = V;
				#endif
			
			}
			T& ret = imgstore.put_image(( typename T::YUYV *) pic,dimx(),dimy());
			free(pic);
			return ret;
		}
		
};

#include "boost/thread/thread.hpp"
#include "boost/thread/mutex.hpp"
/*
	Usage:

	create display and thread:
		CImgDisplayThread tdisp(640,480);
		boost::thread dispthrd(boost::ref(tdisp));
		
	check if still there:
		!tdisp.terminating()
		
	stop thread:
		!tdisp.stop()
		
	putpicture:
		tdisp.putpic(cpic);
		
	wait for thread to join:
		dispthrd.join();
*/



class CImgDisplayThread
{
	boost::timed_mutex * display_lock;
	cimg_library::CImg<uint8_t> * pic;
	volatile bool term;
	cimg_library::CImgDisplay disp;
	public:
		CImgDisplayThread(uint32_t width,uint32_t height ): disp(width,height)
		{
			term = false;
			display_lock = new boost::timed_mutex();
		}
		
		CImgDisplayThread(CImgDisplayThread &c)
		{
			display_lock = new boost::timed_mutex();
			pic = c.pic;
			term = c.term;
			disp = c.disp;
		}
		
		~CImgDisplayThread()
		{}
		
		void stop()
		{
			term = true;
			display_lock->unlock();
		}
		bool terminating()
		{
			return term;
		}
		void putpic(cimg_library::CImg<uint8_t> &p)
		{
			pic = &p;
			display_lock->unlock();
		}
		
		void operator()()
		{
			while(!disp.is_closed && !term)
			{
				display_lock->lock();
				if(!display_lock->timed_lock(boost::posix_time::milliseconds(500)))
				{
					display_lock->unlock();
					continue;
				}
				pic->display(disp);
				display_lock->unlock();
			}
			term = true;
		}
};


#endif
