
#ifndef __HEXDUMP_H__
#define __HEXDUMP_H__

#include <stdio.h>

static inline void hexdump(FILE * file, uint8_t * d, int l)
{
	const int per_line = 8;
	int lines = l / per_line + (l % per_line == 0 ? 0 : 1);

	for (int line = 0; line < lines; line++) {
		fprintf(file, "\t");
		for (int i = line * per_line; i < (line + 1) * per_line; i++) {
			if (i >= l) {
				for (int a = (line + 1) * per_line - i - 1; a >= 0; a--)
					fprintf(file, "   ");
				break;
			}
			fprintf(file, " %02x", (int)d[i]);
		}
		fprintf(file, "   |   ");
		for (int i = line * per_line; i < (line + 1) * per_line && i < l; i++) {
			char show = (d[i] >= 32 && d[i] < 127 ? d[i] : '?');
			fprintf(file, "%c", show);
		}
		fprintf(file, "\n");
	}
}

#endif // __HEXDUMP_H__
